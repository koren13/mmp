<link href="../CSS/style.css" rel="stylesheet" type="text/css"/>
<link href="../CSS/Index.css" rel="stylesheet" type="text/css"/>
<link href="../CSS/test.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MMP</title>
    </head>
    <body>
        <div id="main">
    <div id="header">
      <div id="logo">
      <input type="image" style="width: 450px" style= "height: 450px" style="align: center" src="../slike/logo_mmp.jpg" alt=""/>
      </div>
      <div id="menubar">
        <ul id="menu">
          <li class="selected"><a href="index.php">Vozni redi</a></li>
          <li><a href="../HTML/cenik.html">Cenik</a></li>
          <li><a href="../HTML/mreza_linij.html">Mreža linij</a></li>
          <li><a href="../HTML/obvestila.html">Obvestila</a></li>
          <li> <a href="../HTML/o_strani.html">O spletni strani</a></li>
          <li><a href="../HTML/kontakt.html">Kontakt</a></li>
          
        </ul>
      </div>
    </div>
    <div id="site_content">
      <div class="sidebar">
        <h3>Povezave</h3>
        <p><a href="https://www.facebook.com/MMP-Mariborski-mestni-prevozi-1516327975359920/?fref=ts"><img src="../slike/index.png" alt="" width="42" height="42"/></a>- Facebook</p>
        
        <hr>
        <br>
        <p><a href="https://twitter.com/mestni_prevozi"><img src="../slike/twitter.png" alt="" width="42" height="42"/></a>- Twitter</p>
        
        <h3>Iskanje</h3>
        <form method="post" action="#" id="search_form">
          <p>
            <input class="search" type="text" name="search_field" value="Vnesi besedilo..." />
            <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="../slike/search.png" alt="Search" title="Search" />
          </p>
        </form>
      </div>
      <div id="content">
          <h1>Izberi postajo:</h1>
          <form name="submitPostaj" action="prikaz.php" method="POST">
            <select name="Vstopna">
                <option value="">Vstopna postaja</option>
                
                 <option id="bold" value="">• LINIJA 1 Tezenska Dobrava Smer AP Mlinska - Tezenska dobrava</option>
                 <option value="Melje obracalisce"> - Melje Obracalisce</option>
                 <option value="Meljska Trgovina"> - Meljska Trgovina</option>
                 <option value="Meljska Partizanska">- Meljska Partizanska</option>
                 <option value="AP Mlinska">- AP Mlinska</option>
                 <option value="City Center"> - City Center</option>
                 <option value="Kneza Koclja - Vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                 <option value="Magdalenski Park"> - Magdalenski Park</option>
                 <option value="Ljubljanska - Pariške komune"> - Ljubljanska - Pariške komune</option>
                 <option value="Titova - Nasipna"> - Titova - Nasipna</option>
                 <option value="Ptujska - Tržaška"> - Ptujska - Tržaška</option>
                 <option value="Ptujska - hitra cesta">- Ptujska - hitra cesta</option>
                 <option value="Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="Ptujska - Autocommerce">- Ptujska - Autocommerce</option>
                 <option value="Ptujska - Posta">- Ptujska - Pošta</option>
                 <option value="Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="Dogoska - vrtec">- Dogoška - vrtec</option>
                 <option value="KS Silvira Tomassini">- KS Silvira Tomassini</option>
                 <option value="Tezenska Dobrava - obracalisce">- Tezenska Dobrava - obračalisče</option>

                 
                 <option id="bold" value="LINIJA 2 Betnavska-Razvanje">• LINIJA 2 Betnavska-Razvanje</option>
                 <option value="Melje - obracalisce">- Melje - obračališče</option>
                 <option value="Ul. kraljevica Marka">- Ul. kraljeviča Marka</option> 
                 <option value="Oreško nabrežje 1">- Oreško nabrežje 1</option> 
                 <option value="Oreško nabrežje 2">- Oreško nabrežje 2</option> 
                 <option value="AP Mlinska">- AP Mlinska</option> 
                 <option value="City center">- City center</option> 
                 <option value="Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option> 
                 <option value="Tabor">- Tabor</option> 
                 <option value="Betnavska - Žolgarjeva ">- Betnavska - Žolgarjeva </option> 
                 <option value="Betnavska - Focheva ">- Betnavska - Focheva </option> 
                 <option value="Betnavska - Metelkova">- Betnavska - Metelkova </option> 
                 <option value="Goriška ">- Goriška </option> 
                 <option value="Betnavska - Knafelčeva">- Betnavska - Knafelčeva </option> 
                 <option value="Kardeljeva - Knafelčeva">- Kardeljeva - Knafelčeva</option> 
                 <option value="Kardeljeva - OŠ Tabor I.">- Kardeljeva - OŠ Tabor I.</option> 
                 <option value="Kardeljeva - Borštnikova">- Kardeljeva - Borštnikova</option> 
                 <option value="Betnavski grad Razvanje - vrtnarstvo">- Betnavski grad Razvanje - vrtnarstvo</option> 
                 <option value="Razvanje - obracalisce">- Razvanje - GD Razvanje - obračališče</option> 
                 
                 
                 
                 
                 <option id="bold" value="LINIJA 3 Dobrava-Tezno-Gosposvetska rondo-AP Mlinska-Dobrava">• LINIJA 3 Dobrava-Tezno-Gosposvetska rondo-AP Mlinska-Dobrava</option>
                 <option value="-Pokopopališče Dobrava - vhod"> -Pokopopališče Dobrava - vhod</option>
                 <option value="-Pokopališče Dobrava - obračališče"> -Pokopališče Dobrava - obračališče</option>
                 
                 
                 <option value="LINIJA 4 Studenci"> LINIJA 4 Studenci</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 <option value="-Limbuš - Marof"> -Limbuš - Marof</option>
                 
                 <option value="LINIJA 6 Vzpenjača"> LINIJA 6 Vzpenjača</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value="-Vzpenjača - obračališče"> -Vzpenjača - obračališče</option>
                 
                 <option value="LINIJA 7 Kamnica"> LINIJA 7 Kamnica</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value=" -Rošpoh - obračališče">  -Rošpoh - obračališče</option>
                 
                 <option value="LINIJA ŠT 8: AP Mlinska -Terme Fontana "> LINIJA ŠT 8: AP Mlinska -Terme Fontana</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 <option value="-Terme Fontana "> -Terme Fontana </option>
                 
                 <option value="LINIJA 9 Zrkovci-Dogoše "> LINIJA 9 Zrkovci-Dogoše</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 <option value="-Dupleška cesta 255 ">  -Dupleška cesta 255 </option>
                 
                 <option value="LINIJA 10 Malečnik "> LINIJA 10 Malečnik</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value="-Metava - obračališče "> -Metava - obračališče </option>
                 
                 <option value="LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava "> LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava</option>
                 <option value="-Pokopališče Dobrava - obračališče"> -Pokopališče Dobrava - obračališče</option>
                 <option value="-Dupleška - kanal "> -Dupleška - kanal </option>
                 
                 <option value="LINIJA 13 Črnogorska "> LINIJA 13 Črnogorska</option>
                 <option value="Poštni center - obračališče"> -Poštni center - obračališče</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 
                 <option value="LINIJA 15 Bresternica "> LINIJA 15 Bresternica </option>
                 <option value="-Bresternica - obračališče"> -Bresternica - obračališče</option>
                 <option value="-Košaški dol">  -Košaški dol</option>
                 
                 <option value="LINIJA 151 Gaj nad Mariborom "> LINIJA 151 Gaj nad Mariborom </option>
                 <option value="-Gaj nad Mariborom"> -Gaj nad Mariborom</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 
                   <option value=" Linija 16 LINIJA 16 Dogoše-Zg. Duplek "> LINIJA 16 Dogoše-Zg. Duplek</option>
                 <option value=" Linija 16 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 16 -City Center"> -City center</option>
                 <option value=" Linija 16 -Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value=" Linija 16 -Magdalena"> -Magdalena</option>
                 <option value=" Linija 16 -Pobre�ka - Europark"> -Pobre�ka - Europark</option>
                 <option value=" Linija 16 -Greenwich"> -Greenwich</option>
                 <option value=" Linija 16 -�ufarjeva - TVD Partizan"> -�ufarjeva - TVD Partizan</option>
                 <option value=" Linija 16 -�ufarjeva - dom st. ob�anov"> -�ufarjeva - dom st. ob�anov</option>
                 <option value=" Linija 16 -Veljka Vlahovi�a - S31"> -Veljka Vlahovi�a - S31</option>
                 <option value=" Linija 16 -Veljka Vlahovi�a - trg. center"> -Veljka Vlahovi�a - trg. center</option>
                 <option value=" Linija 16 -Cesta XIV.divizije - vrtnarstvo"> -Cesta XIV.divizije - vrtnarstvo</option>
                 <option value=" Linija 16 -Duple�ka - Tezenska"> -Duple�ka - Tezenska</option>
                 <option value=" Linija 16 -Brezje"> -Brezje</option>
                 <option value=" Linija 16 -Duple�ka - Jar�eva"> -Duple�ka - Jar�eva</option>
                 <option value=" Linija 16 -Duple�ka - kanal"> -Duple�ka - kanal</option>
                 <option value=" Linija 16 -Dogo�e - GD"> -Dogo�e - GD</option>
                 <option value=" Linija 16 -Dogo�e - polje"> -Dogo�e - polje</option>
                 <option value=" Linija 16 -Dogo�e - obra�ali��e"> -Dogo�e - obra�ali��e</option>
                 <option value=" Linija 16 -Zg.Duplek - obračališče"> -Zg.Duplek - obračališče</option>
                 
                 <option value=" LINIJA 17: Ribniško selo - Studenci "> LINIJA 17: Ribniško selo - Studenci </option>
                 <option value=" Linija 17 -Ribniško selo - obračališče">-Ribniško selo - obračališče</option>
                 <option value=" Linija 17 -Ribnik 1"> -Ribnik 1</option>
                 <option value=" Linija 17 -Kli�ek"> -Kli�ek</option>
                 <option value=" Linija 17 -Akvarij"> -Akvarij</option>
                 <option value=" Linija 17 -Krekova - ob�ina"> -Krekova - ob�ina</option>
                 <option value=" Linija 17 -Krekova"> -Krekova</option>
                 <option value=" Linija 17 -Strossmayerjeva"> -Strossmayerjeva</option>
                 <option value=" Linija 17 -Koro�ka - Po�tna"> -Koro�ka - Po�tna</option>
                 <option value=" Linija 17 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 17 -City center"> -City center</option>
                 <option value=" Linija 17 -Magdalena"> -Magdalena</option>
                 <option value=" Linija 17 -Magdalenski park"> -Magdalenski park</option>
                 <option value=" Linija 17 -Ljubljanska - Pari�ke komune"> -Ljubljanska - Pari�ke komune</option>
                 <option value=" Linija 17 -Pari�ke Komune"> -Pari�ke Komune</option>
                 <option value=" Linija 17 -Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
                 <option value=" Linija 17 -Na Poljanah - Planet Tu�"> -Na Poljanah - Planet Tu�</option>
                 <option value=" Linija 17 -�arhova - po�ta"> -�arhova - po�ta</option>
                 <option value=" Linija 17 -�arhova - obra�ali��e"> -�arhova - obra�ali��e</option>
                 <option value=" Linija 17 -Erjav�eva - ZD"> -Erjav�eva - ZD</option>
                 <option value=" Linija 17 -Erjav�eva"> -Erjav�eva</option>
                 <option value=" Linija 17 -Studenci - obra�ali��e"> -Studenci - obra�ali��e</option>
                 
                 <option value="LINIJA 18 Pekre "> LINIJA 18 Pekre</option>
                 <option value=" Linija 18 -Melje - obračališče"> -Melje - obračališče</option>
                 <option value=" Linija 18 -Meljska - trgovina"> -Meljska - trgovina</option>
                 <option value=" Linija 18 -Meljska - Partizanska"> -Meljska - Partizanska</option>
                 <option value=" Linija 18 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 18 -City center"> -City center</option>
                 <option value=" Linija 18 -Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value=" Linija 18 -Tabor"> -Tabor</option>
                 <option value=" Linija 18 -Gorkega -  Preradovi�eva"> -Gorkega -  Preradovi�eva</option>
                 <option value=" Linija 18 -Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
                 <option value=" Linija 18 -Engelsova - �arhova"> -Engelsova - �arhova</option>
                 <option value=" Linija 18 -Engelsova - voja�nica"> -Engelsova - voja�nica</option>
                 <option value=" Linija 18 -Prol.brigad - rond"> -Prol.brigad - rond</option>
                 <option value=" Linija 18 -Nova vas 2"> -Nova vas 2</option>
                 <option value=" Linija 18 -Ul. Poh. odreda - O� G. �iliha"> -Ul. Poh. odreda - O� G. �iliha</option>
                 <option value=" Linija 18 -Ul. Poh. odreda - O� L. Pliber�ek"> -Ul. Poh. odreda - O� L. Pliber�ek</option>
                 <option value=" Linija 18 -Lackova - Stara lipa"> -Lackova - Stara lipa</option>
                 <option value=" Linija 18 -Lackova - Mlada lipa"> -Lackova - Mlada lipa</option>
                 <option value=" Linija 18 -Lackova - Na gorco"> -Lackova - Na gorco</option>
                 <option value=" Linija 18 -Pekre - trgovina"> -Pekre - trgovinae </option>
                 
                 
                 <option value="LINIJA 19 Šarhova ">LINIJA 19 Šarhova</option>
                 <option value=" Linija 19 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 19 -City center"> -City center</option>
                 <option value=" Linija 19 -Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value=" Linija 19 -Koro�ka - Po�tna"> -Koro�ka - Po�tna</option>
                 <option value=" Linija 19 -Strossmayerjeva"> -Strossmayerjeva</option>
                 <option value=" Linija 19 -Tretja gimnazija"> -Tretja gimnazija</option>
                 <option value=" Linija 19 -Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option>
                 <option value=" Linija 19 -Gosposvetska - Turnerjeva"> -Gosposvetska - Turnerjeva</option>
                 <option value=" Linija 19 -Gosposvetska - rondo"> -Gosposvetska - rondo</option>
                 <option value=" Linija 19 -Dravograjska - Sokolska"> -Dravograjska - Sokolska</option>
                 <option value=" Linija 19 -Dravograjska - Poljane"> -Dravograjska - Poljane</option>
                 <option value=" Linija 19 -Dravograjska - I. internacionale"> -Dravograjska - I. internacionale</option>
                 <option value=" Linija 19 -Vrtnarska - Qlandia"> -Vrtnarska - Qlandia</option>
                 <option value=" Linija 19 -Ilichova - RTV"> -Ilichova - RTV</option>
                 <option value=" Linija 19 -Ilichova - Korbunova obra�ali��e"> -Ilichova - Korbunova obra�ali��e</option>
                 <option value=" Linija 19 -Korbunova - Kamen�kova"> -Korbunova - Kamen�kova</option>
                 <option value=" Linija 19 -�arhova - obra�ali��e"> -�arhova - obra�ali��e</option>
                 
                 
                  <option value="LINIJA 20 Grušova "> LINIJA 20 Grušova</option>
                 <option value=" Linija 20 -Melje - obračališče"> -Melje - obračališče</option>                 
                 <option value=" Linija 20 -Ul. kraljevi�a Marka"> -Ul. kraljevi�a Marka</option>
                 <option value=" Linija 20 -Ore�ko nabre�je 1"> -Ore�ko nabre�je 1</option>
                 <option value=" Linija 20 -Ore�ko nabre�je 2"> -Ore�ko nabre�je 2</option>


                 <option value=" Linija 20 -Meljska - trgovina"> -Meljska - trgovina</option>
                 <option value=" Linija 20 -Meljska - Partizanska"> -Meljska - Partizanska</option>


                 <option value=" Linija 20 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 20 -City center"> -City center</option>
                 <option value=" Linija 20 -Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>


                 <option value=" Linija 20 -Glavni trg � �idovska"> -Glavni trg � �idovska</option>


                 <option value=" Linija 20 -Magdalena"> -Magdalena</option>
                 <option value=" Linija 20 -Pobre�ka � Europark"> -Pobre�ka � Europark</option>
                 <option value=" Linija 20 -Greenwich"> -Greenwich</option>
                 <option value=" Linija 20 -�ufarjeva - TVD Partizan"> -�ufarjeva - TVD Partizan</option>
                 <option value=" Linija 20 -�ufarjeva - dom st. ob�anov"> -�ufarjeva - dom st. ob�anov</option>
                 <option value=" Linija 20 -Meljski hrib � most"> -Meljski hrib � most</option>
                 <option value=" Linija 20 -Kova�i�"> -Kova�i�</option>
                 <option value=" Linija 20 -Lorber"> -Lorber</option>
                 <option value=" Linija 20 -Male�nik - odcep Tr�ova"> -Male�nik - odcep Tr�ova</option>
                 <option value=" Linija 20 -Male�nik � trgovina"> -Male�nik � trgovina</option>
                 <option value=" Linija 20 -Tr�ova 31a"> -Tr�ova 31a</option>
                 <option value=" Linija 20 -Tr�ova � Gri�ek"> -Tr�ova � Gri�ek</option>
                 <option value=" Linija 20 -Novek"> -Novek</option>
                 <option value=" Linija 20 -Metava - Duplek kri�i��e"> -Metava - Duplek kri�i��e</option>
                 <option value=" Linija 20 -Metava � obra�ali��e"> -Metava � obra�ali��e</option>


                 <option value=" Linija 20 -Metava - Duplek kri�i��e"> -Metava - Duplek kri�i��e</option>
                 <option value=" Linija 20 -Novak"> -Novak</option>
                 <option value=" Linija 20 -Tr�ova � Gri�ek"> -Tr�ova � Gri�ek</option>
                 <option value=" Linija 20 -Tr�ova 31a"> -Tr�ova 31a</option>
                 <option value=" Linija 20 -Male�nik � trgovina"> -Male�nik � trgovina</option>
                 <option value=" Linija 20 -Male�nik - odcep Tr�ova"> -Male�nik - odcep Tr�ova</option>


                 <option value=" Linija 20 -Celestrina"> -Celestrina</option>
                 <option value=" Linija 20 -Nebova I"> -Nebova I</option>
                 <option value=" Linija 20 -Nebova II"> -Nebova II</option>
                 <option value=" Linija 20 -Ruper�e"> -Ruper�e</option>
                 <option value=" Linija 20 -Kronaveter"> -Kronaveter</option>
                 <option value=" Linija 20 -Knezar"> -Knezar</option>
                 <option value=" Linija 20 -Metava � odcep"> -Metava � odcep</option>
                 <option value=" Linija 20 -Zimica � odcep"> -Zimica � odcep</option>
                 <option value=" Linija 20 -Gru�ova 1"> -Gru�ova 1</option>                 
                 <option value=" Linija 20  -Grušova-obračališče">  -Grušova-obračališče </option>
                 
                
                 
                 <option value="LINIJA 21 Ljubljanska-Tržaška c. - Merkur "> LINIJA 21 Ljubljanska-Tržaška c. - Merkur</option>
                 <option value=" Linija 21 -Melje - obračališče"> -Melje - obračališče</option>                 
                 <option value=" Linija 21 -Ul. kraljevi�a Marka"> -Ul. kraljevi�a Marka</option>
                 <option value=" Linija 21 -Ore�ko nabre�je 1"> -Ore�ko nabre�je 1</option>
                 <option value=" Linija 21 -Ore�ko nabre�je 2"> -Ore�ko nabre�je 2</option>
                 <option value=" Linija 21 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 21 -City center"> -City center</option>
                 <option value=" Linija 21 -Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value=" Linija 21 -Magdalenski park"> -Magdalenski park</option>
                 <option value=" Linija 21 -Ljubljanska - Pari�ke komune"> -Ljubljanska - Pari�ke komune</option>
                 <option value=" Linija 21 -Ljubljanska - Focheva"> -Ljubljanska - Focheva</option>
                 <option value=" Linija 21 -Ljubljanska - trgovski center"> -Ljubljanska - trgovski center</option>
                 <option value=" Linija 21 -Ljubljanska - stolpnica"> -Ljubljanska - stolpnica</option>
                 <option value=" Linija 21 -Ljubljanska 140"> -Ljubljanska 140</option>
                 <option value=" Linija 21 -Ul. Eve Lov�e - Betnava"> -Ul. Eve Lov�e - Betnava</option>
                 <option value=" Linija 21 -Ul. Eve Lov�e - Energetika"> -Ul. Eve Lov�e - Energetika</option>
                 <option value=" Linija 21 -Ul. Eve Lov�e - Lidl"> -Ul. Eve Lov�e - Lidl</option>
                 <option value=" Linija 21 -Tr�a�ka cesta - Elko"> -Tr�a�ka cesta - Elko</option>
                 <option value=" Linija 21 -Tr�a�ka cesta - Semenarna"> -Tr�a�ka cesta - Semenarna</option>
                 <option value=" Linija 21 -Tr�a�ka cesta - Carinarnica"> -Tr�a�ka cesta - Carinarnica</option>
                 <option value=" Linija 21 -Tr�a�ka cesta - Rutar"> -Tr�a�ka cesta - Rutar</option>
                 <option value=" Linija 21 -E'Leclerc - obra�ali��e"> -E'Leclerc - obra�ali��e</option>
                 <option value=" Linija 21 -Tr�a�ka cesta - Bauhaus"> -Tr�a�ka cesta - Bauhaus</option>
                 <option value=" Linija 21 - Tržaška cesta - Merkur">  -Tržaška cesta - Merkur </option>
                </select>
 
          <div>
              <img src="../slike/arrow1.png" alt="" id="arrow1"/>
          </div>
          
            <select name="Izstopna">
                 <option value="">Izstopna postaja</option>
                  <option id="bold" value="">• LINIJA 1 Tezenska Dobrava Smer AP Mlinska - Tezenska dobrava</option>
                 <option value="Melje obracalisce"> - Melje Obracalisce</option>
                 <option value="Meljska Trgovina"> - Meljska Trgovina</option>
                 <option value="Meljska Partizanska">- Meljska Partizanska</option>
                 <option value="AP Mlinska">- AP Mlinska</option>
                 <option value="City Center"> - City Center</option>
                 <option value="Kneza Koclja - Vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                 <option value="Magdalenski Park"> - Magdalenski Park</option>
                 <option value="Ljubljanska - Pariške komune"> - Ljubljanska - Pariške komune</option>
                 <option value="Titova - Nasipna"> - Titova - Nasipna</option>
                 <option value="Ptujska - Tržaška"> - Ptujska - Tržaška</option>
                 <option value="Ptujska - hitra cesta">- Ptujska - hitra cesta</option>
                 <option value="Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="Ptujska - Autocommerce">- Ptujska - Autocommerce</option>
                 <option value="Ptujska - Posta">- Ptujska - Pošta</option>
                 <option value="Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="Dogoska - vrtec">- Dogoška - vrtec</option>
                 <option value="KS Silvira Tomassini">- KS Silvira Tomassini</option>
                 <option value="Tezenska Dobrava - obracalisce">- Tezenska Dobrava - obračalisče</option>
                 
                 
                <option id="bold" value="LINIJA 2 Betnavska-Razvanje">• LINIJA 2 Betnavska-Razvanje</option>
                 <option value="Melje - obracalisce">- Melje - obračališče</option>
                 <option value="Ul. kraljevica Marka">- Ul. kraljeviča Marka</option> 
                 <option value="Oreško nabrežje 1">- Oreško nabrežje 1</option> 
                 <option value="Oreško nabrežje 2">- Oreško nabrežje 2</option> 
                 <option value="AP Mlinska">- AP Mlinska</option> 
                 <option value="City center">- City center</option> 
                 <option value="Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option> 
                 <option value="Tabor">- Tabor</option> 
                 <option value="Betnavska - Žolgarjeva ">- Betnavska - Žolgarjeva </option> 
                 <option value="Betnavska - Focheva ">- Betnavska - Focheva </option> 
                 <option value="Betnavska - Metelkova">- Betnavska - Metelkova </option> 
                 <option value="Goriška ">- Goriška </option> 
                 <option value="Betnavska - Knafelčeva">- Betnavska - Knafelčeva </option> 
                 <option value="Kardeljeva - Knafelčeva">- Kardeljeva - Knafelčeva</option> 
                 <option value="Kardeljeva - OŠ Tabor I.">- Kardeljeva - OŠ Tabor I.</option> 
                 <option value="Kardeljeva - Borštnikova">- Kardeljeva - Borštnikova</option> 
                 <option value="Betnavski grad Razvanje - vrtnarstvo">- Betnavski grad Razvanje - vrtnarstvo</option> 
                 <option value="Razvanje - obracalisce">- Razvanje - GD Razvanje - obračališče</option> 
                 
                 
                 <option value="LINIJA 3 Dobrava-Tezno-Gosposvetska rondo-AP Mlinska-Dobrava">LINIJA 3 Dobrava-Tezno-Gosposvetska rondo-AP Mlinska-Dobrava</option>
                 <option value="-Pokopopališče Dobrava - vhod">-Pokopopališče Dobrava - vhod</option>
                 <option value="-Pokopališče Dobrava - obračališče"> -Pokopališče Dobrava - obračališče</option>
                 
                 
                 <option value="LINIJA 4 Studenci">LINIJA 4 Studenci</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 <option value="-Limbuš - Marof"> -Limbuš - Marof</option>
                 
                 
                 <option value="LINIJA 6 Vzpenjača"> "LINIJA 6 Vzpenjača</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value="-Vzpenjača - obračališče"> -Vzpenjača - obračališče</option>
                 
                 
                 <option value="LINIJA 7 Kamnica"> "LINIJA 7 Kamnica</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value=" -Rošpoh - obračališče">  -Rošpoh - obračališče</option>
                 
                 
                 <option value="LINIJA ŠT 8: AP Mlinska -Terme Fontana ">LINIJA ŠT 8: AP Mlinska -Terme Fontana</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 <option value="-Terme Fontana "> -Terme Fontana </option>
                 
                 
                 <option value="LINIJA 9 Zrkovci-Dogoše "> LINIJA 9 Zrkovci-Dogoše</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 <option value="-Dupleška cesta 255 ">  -Dupleška cesta 255 </option>
                 
                 
                  <option value="LINIJA 10 Malečnik "> LINIJA 10 Malečnik</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value="-Metava - obračališče "> -Metava - obračališče </option>
                 
                 
                 <option value="LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava "> "LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava</option>
                 <option value="-Pokopališče Dobrava - obračališče"> -Pokopališče Dobrava - obračališče</option>
                 <option value="-Dupleška - kanal "> -Dupleška - kanal </option>
                 
                 
                 <option value="LINIJA 13 Črnogorska "> LINIJA 13 Črnogorska</option>
                 <option value="-Poštni center - obračališče"> -Poštni center - obračališče</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 
                 
                 <option value="LINIJA 15 Bresternica "> LINIJA 15 Bresternica </option>
                 <option value="-Bresternica - obračališče">-Bresternica - obračališče</option>
                 <option value=" -Košaški dol"> -Košaški dol</option>
                 
                 
                 <option value="LINIJA 151 Gaj nad Mariborom ">LINIJA 151 Gaj nad Mariborom </option>
                 <option value=" -Gaj nad Mariborom"> -Gaj nad Mariborom</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 
                 
                 <option value=" Linija 16 LINIJA 16 Dogoše-Zg. Duplek "> LINIJA 16 Dogoše-Zg. Duplek</option>
                 <option value=" Linija 16 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 16 -City Center"> -City center</option>
                 <option value=" Linija 16 -Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value=" Linija 16 -Magdalena"> -Magdalena</option>
                 <option value=" Linija 16 -Pobre�ka - Europark"> -Pobre�ka - Europark</option>
                 <option value=" Linija 16 -Greenwich"> -Greenwich</option>
                 <option value=" Linija 16 -�ufarjeva - TVD Partizan"> -�ufarjeva - TVD Partizan</option>
                 <option value=" Linija 16 -�ufarjeva - dom st. ob�anov"> -�ufarjeva - dom st. ob�anov</option>
                 <option value=" Linija 16 -Veljka Vlahovi�a - S31"> -Veljka Vlahovi�a - S31</option>
                 <option value=" Linija 16 -Veljka Vlahovi�a - trg. center"> -Veljka Vlahovi�a - trg. center</option>
                 <option value=" Linija 16 -Cesta XIV.divizije - vrtnarstvo"> -Cesta XIV.divizije - vrtnarstvo</option>
                 <option value=" Linija 16 -Duple�ka - Tezenska"> -Duple�ka - Tezenska</option>
                 <option value=" Linija 16 -Brezje"> -Brezje</option>
                 <option value=" Linija 16 -Duple�ka - Jar�eva"> -Duple�ka - Jar�eva</option>
                 <option value=" Linija 16 -Duple�ka - kanal"> -Duple�ka - kanal</option>
                 <option value=" Linija 16 -Dogo�e - GD"> -Dogo�e - GD</option>
                 <option value=" Linija 16 -Dogo�e - polje"> -Dogo�e - polje</option>
                 <option value=" Linija 16 -Dogo�e - obra�ali��e"> -Dogo�e - obra�ali��e</option>
                 <option value=" Linija 16 -Zg.Duplek - obračališče"> -Zg.Duplek - obračališče</option>
                 
                 <option value=" LINIJA 17: Ribniško selo - Studenci "> LINIJA 17: Ribniško selo - Studenci </option>
                 <option value=" Linija 17 -Ribniško selo - obračališče">-Ribniško selo - obračališče</option>
                 <option value=" Linija 17 -Ribnik 1"> -Ribnik 1</option>
                 <option value=" Linija 17 -Kli�ek"> -Kli�ek</option>
                 <option value=" Linija 17 -Akvarij"> -Akvarij</option>
                 <option value=" Linija 17 -Krekova - ob�ina"> -Krekova - ob�ina</option>
                 <option value=" Linija 17 -Krekova"> -Krekova</option>
                 <option value=" Linija 17 -Strossmayerjeva"> -Strossmayerjeva</option>
                 <option value=" Linija 17 -Koro�ka - Po�tna"> -Koro�ka - Po�tna</option>
                 <option value=" Linija 17 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 17 -City center"> -City center</option>
                 <option value=" Linija 17 -Magdalena"> -Magdalena</option>
                 <option value=" Linija 17 -Magdalenski park"> -Magdalenski park</option>
                 <option value=" Linija 17 -Ljubljanska - Pari�ke komune"> -Ljubljanska - Pari�ke komune</option>
                 <option value=" Linija 17 -Pari�ke Komune"> -Pari�ke Komune</option>
                 <option value=" Linija 17 -Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
                 <option value=" Linija 17 -Na Poljanah - Planet Tu�"> -Na Poljanah - Planet Tu�</option>
                 <option value=" Linija 17 -�arhova - po�ta"> -�arhova - po�ta</option>
                 <option value=" Linija 17 -�arhova - obra�ali��e"> -�arhova - obra�ali��e</option>
                 <option value=" Linija 17 -Erjav�eva - ZD"> -Erjav�eva - ZD</option>
                 <option value=" Linija 17 -Erjav�eva"> -Erjav�eva</option>
                 <option value=" Linija 17 -Studenci - obra�ali��e"> -Studenci - obra�ali��e</option>
                 
                 <option value="LINIJA 18 Pekre "> LINIJA 18 Pekre</option>
                 <option value=" Linija 18 -Melje - obračališče"> -Melje - obračališče</option>
                 <option value=" Linija 18 -Meljska - trgovina"> -Meljska - trgovina</option>
                 <option value=" Linija 18 -Meljska - Partizanska"> -Meljska - Partizanska</option>
                 <option value=" Linija 18 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 18 -City center"> -City center</option>
                 <option value=" Linija 18 -Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value=" Linija 18 -Tabor"> -Tabor</option>
                 <option value=" Linija 18 -Gorkega -  Preradovi�eva"> -Gorkega -  Preradovi�eva</option>
                 <option value=" Linija 18 -Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
                 <option value=" Linija 18 -Engelsova - �arhova"> -Engelsova - �arhova</option>
                 <option value=" Linija 18 -Engelsova - voja�nica"> -Engelsova - voja�nica</option>
                 <option value=" Linija 18 -Prol.brigad - rond"> -Prol.brigad - rond</option>
                 <option value=" Linija 18 -Nova vas 2"> -Nova vas 2</option>
                 <option value=" Linija 18 -Ul. Poh. odreda - O� G. �iliha"> -Ul. Poh. odreda - O� G. �iliha</option>
                 <option value=" Linija 18 -Ul. Poh. odreda - O� L. Pliber�ek"> -Ul. Poh. odreda - O� L. Pliber�ek</option>
                 <option value=" Linija 18 -Lackova - Stara lipa"> -Lackova - Stara lipa</option>
                 <option value=" Linija 18 -Lackova - Mlada lipa"> -Lackova - Mlada lipa</option>
                 <option value=" Linija 18 -Lackova - Na gorco"> -Lackova - Na gorco</option>
                 <option value=" Linija 18 -Pekre - trgovina"> -Pekre - trgovinae </option>
                 
                 
                 <option value="LINIJA 19 Šarhova ">LINIJA 19 Šarhova</option>
                 <option value=" Linija 19 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 19 -City center"> -City center</option>
                 <option value=" Linija 19 -Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value=" Linija 19 -Koro�ka - Po�tna"> -Koro�ka - Po�tna</option>
                 <option value=" Linija 19 -Strossmayerjeva"> -Strossmayerjeva</option>
                 <option value=" Linija 19 -Tretja gimnazija"> -Tretja gimnazija</option>
                 <option value=" Linija 19 -Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option>
                 <option value=" Linija 19 -Gosposvetska - Turnerjeva"> -Gosposvetska - Turnerjeva</option>
                 <option value=" Linija 19 -Gosposvetska - rondo"> -Gosposvetska - rondo</option>
                 <option value=" Linija 19 -Dravograjska - Sokolska"> -Dravograjska - Sokolska</option>
                 <option value=" Linija 19 -Dravograjska - Poljane"> -Dravograjska - Poljane</option>
                 <option value=" Linija 19 -Dravograjska - I. internacionale"> -Dravograjska - I. internacionale</option>
                 <option value=" Linija 19 -Vrtnarska - Qlandia"> -Vrtnarska - Qlandia</option>
                 <option value=" Linija 19 -Ilichova - RTV"> -Ilichova - RTV</option>
                 <option value=" Linija 19 -Ilichova - Korbunova obra�ali��e"> -Ilichova - Korbunova obra�ali��e</option>
                 <option value=" Linija 19 -Korbunova - Kamen�kova"> -Korbunova - Kamen�kova</option>
                 <option value=" Linija 19 -�arhova - obra�ali��e"> -�arhova - obra�ali��e</option>
                 
                 
                  <option value="LINIJA 20 Grušova "> LINIJA 20 Grušova</option>
                 <option value=" Linija 20 -Melje - obračališče"> -Melje - obračališče</option>                 
                 <option value=" Linija 20 -Ul. kraljevi�a Marka"> -Ul. kraljevi�a Marka</option>
                 <option value=" Linija 20 -Ore�ko nabre�je 1"> -Ore�ko nabre�je 1</option>
                 <option value=" Linija 20 -Ore�ko nabre�je 2"> -Ore�ko nabre�je 2</option>


                 <option value=" Linija 20 -Meljska - trgovina"> -Meljska - trgovina</option>
                 <option value=" Linija 20 -Meljska - Partizanska"> -Meljska - Partizanska</option>


                 <option value=" Linija 20 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 20 -City center"> -City center</option>
                 <option value=" Linija 20 -Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>


                 <option value=" Linija 20 -Glavni trg � �idovska"> -Glavni trg � �idovska</option>


                 <option value=" Linija 20 -Magdalena"> -Magdalena</option>
                 <option value=" Linija 20 -Pobre�ka � Europark"> -Pobre�ka � Europark</option>
                 <option value=" Linija 20 -Greenwich"> -Greenwich</option>
                 <option value=" Linija 20 -�ufarjeva - TVD Partizan"> -�ufarjeva - TVD Partizan</option>
                 <option value=" Linija 20 -�ufarjeva - dom st. ob�anov"> -�ufarjeva - dom st. ob�anov</option>
                 <option value=" Linija 20 -Meljski hrib � most"> -Meljski hrib � most</option>
                 <option value=" Linija 20 -Kova�i�"> -Kova�i�</option>
                 <option value=" Linija 20 -Lorber"> -Lorber</option>
                 <option value=" Linija 20 -Male�nik - odcep Tr�ova"> -Male�nik - odcep Tr�ova</option>
                 <option value=" Linija 20 -Male�nik � trgovina"> -Male�nik � trgovina</option>
                 <option value=" Linija 20 -Tr�ova 31a"> -Tr�ova 31a</option>
                 <option value=" Linija 20 -Tr�ova � Gri�ek"> -Tr�ova � Gri�ek</option>
                 <option value=" Linija 20 -Novek"> -Novek</option>
                 <option value=" Linija 20 -Metava - Duplek kri�i��e"> -Metava - Duplek kri�i��e</option>
                 <option value=" Linija 20 -Metava � obra�ali��e"> -Metava � obra�ali��e</option>


                 <option value=" Linija 20 -Metava - Duplek kri�i��e"> -Metava - Duplek kri�i��e</option>
                 <option value=" Linija 20 -Novak"> -Novak</option>
                 <option value=" Linija 20 -Tr�ova � Gri�ek"> -Tr�ova � Gri�ek</option>
                 <option value=" Linija 20 -Tr�ova 31a"> -Tr�ova 31a</option>
                 <option value=" Linija 20 -Male�nik � trgovina"> -Male�nik � trgovina</option>
                 <option value=" Linija 20 -Male�nik - odcep Tr�ova"> -Male�nik - odcep Tr�ova</option>


                 <option value=" Linija 20 -Celestrina"> -Celestrina</option>
                 <option value=" Linija 20 -Nebova I"> -Nebova I</option>
                 <option value=" Linija 20 -Nebova II"> -Nebova II</option>
                 <option value=" Linija 20 -Ruper�e"> -Ruper�e</option>
                 <option value=" Linija 20 -Kronaveter"> -Kronaveter</option>
                 <option value=" Linija 20 -Knezar"> -Knezar</option>
                 <option value=" Linija 20 -Metava � odcep"> -Metava � odcep</option>
                 <option value=" Linija 20 -Zimica � odcep"> -Zimica � odcep</option>
                 <option value=" Linija 20 -Gru�ova 1"> -Gru�ova 1</option>                 
                 <option value=" Linija 20  -Grušova-obračališče">  -Grušova-obračališče </option>
                 
                
                 
                 <option value="LINIJA 21 Ljubljanska-Tržaška c. - Merkur "> LINIJA 21 Ljubljanska-Tržaška c. - Merkur</option>
                 <option value=" Linija 21 -Melje - obračališče"> -Melje - obračališče</option>                 
                 <option value=" Linija 21 -Ul. kraljevi�a Marka"> -Ul. kraljevi�a Marka</option>
                 <option value=" Linija 21 -Ore�ko nabre�je 1"> -Ore�ko nabre�je 1</option>
                 <option value=" Linija 21 -Ore�ko nabre�je 2"> -Ore�ko nabre�je 2</option>
                 <option value=" Linija 21 -AP Mlinska"> -AP Mlinska</option>
                 <option value=" Linija 21 -City center"> -City center</option>
                 <option value=" Linija 21 -Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value=" Linija 21 -Magdalenski park"> -Magdalenski park</option>
                 <option value=" Linija 21 -Ljubljanska - Pari�ke komune"> -Ljubljanska - Pari�ke komune</option>
                 <option value=" Linija 21 -Ljubljanska - Focheva"> -Ljubljanska - Focheva</option>
                 <option value=" Linija 21 -Ljubljanska - trgovski center"> -Ljubljanska - trgovski center</option>
                 <option value=" Linija 21 -Ljubljanska - stolpnica"> -Ljubljanska - stolpnica</option>
                 <option value=" Linija 21 -Ljubljanska 140"> -Ljubljanska 140</option>
                 <option value=" Linija 21 -Ul. Eve Lov�e - Betnava"> -Ul. Eve Lov�e - Betnava</option>
                 <option value=" Linija 21 -Ul. Eve Lov�e - Energetika"> -Ul. Eve Lov�e - Energetika</option>
                 <option value=" Linija 21 -Ul. Eve Lov�e - Lidl"> -Ul. Eve Lov�e - Lidl</option>
                 <option value=" Linija 21 -Tr�a�ka cesta - Elko"> -Tr�a�ka cesta - Elko</option>
                 <option value=" Linija 21 -Tr�a�ka cesta - Semenarna"> -Tr�a�ka cesta - Semenarna</option>
                 <option value=" Linija 21 -Tr�a�ka cesta - Carinarnica"> -Tr�a�ka cesta - Carinarnica</option>
                 <option value=" Linija 21 -Tr�a�ka cesta - Rutar"> -Tr�a�ka cesta - Rutar</option>
                 <option value=" Linija 21 -E'Leclerc - obra�ali��e"> -E'Leclerc - obra�ali��e</option>
                 <option value=" Linija 21 -Tr�a�ka cesta - Bauhaus"> -Tr�a�ka cesta - Bauhaus</option>
                 <option value=" Linija 21 - Tržaška cesta - Merkur">  -Tržaška cesta - Merkur </option>
            </select>
                  <br/><input type="submit" value="Potrdi" name="submit" id="gumb" />
      
        </form>   
             <input type="submit" value="Priljubljene Postaje" name="submit" id="gumb" />
    </body>
      </div>
    </div>
    <div id="footer">
      Copyright &copy; MMP 
          <script type="text/javascript">
             document.write ('<p><span id="date-time">', new Date().toLocaleString(), '<\/span>.<\/p>')
             if (document.getElementById) onload = function () {
	               setInterval ("document.getElementById ('date-time').firstChild.data = new Date().toLocaleString()", 50)
}
</script>
    </div>
  </div>
    </body>
</html>

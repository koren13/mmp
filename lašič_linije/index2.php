
<link href="../CSS/Index.css" rel="stylesheet" type="text/css"/>
<link href="../CSS/style.css" rel="stylesheet" type="text/css"/>
<link href="../CSS/prvastran.css" rel="stylesheet" type="text/css"/>
<link href="../CSS/test.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MMP</title>
      </head>
      <body>
     <div id="main">
      <div id="header">
      <div id="logo">
      <input type="image" style="width: 450px" style= "height: 450px" style="align: center" src="../slike/logo_mmp.jpg" alt=""/>
      </div>
      <div id="menubar">
        <ul id="menu">
          <li class="selected"><a href="index.php">Vozni redi</a></li>
          <li><a href="../HTML/cenik.html">Cenik</a></li>
          <li><a href="../HTML/mreza_linij.html">Mreža linij</a></li>
          <li><a href="../HTML/obvestila.html">Obvestila</a></li>
          <li> <a href="../HTML/o_strani.html">O spletni strani</a></li>
          <li><a href="../HTML/kontakt.html">Kontakt</a></li>
          
        </ul>
      </div>
    </div>
    <div id="site_content">
      <div class="sidebar">
        <h3>Povezave</h3>
        <p><a href="https://www.facebook.com/MMP-Mariborski-mestni-prevozi-1516327975359920/?fref=ts"><img src="../slike/index.png" alt="" width="42" height="42"/></a>- Facebook</p>
        
        <hr>
        <br>
        <p><a href="https://twitter.com/mestni_prevozi"><img src="../slike/twitter.png" alt="" width="42" height="42"/></a>- Twitter</p>
        
        <h3>Iskanje</h3>
        <form method="post" action="#" id="search_form">
          <p>
            <input class="search" type="text" name="search_field" value="Vnesi besedilo..." />
            <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="../slike/search.png" alt="Search" title="Search" />
          </p>
        </form>
      </div>
       <div id="content">
          <h1>Izberi postajo:</h1>
          <form name="submitPostaj" action="prikaz_2.php" method="POST">
            <select name="Vstopna">
                <option value="Vstopna">Vstopna postaja</option>
                 
                <option id="bold" value="Linija 1">•LINIJA 1 Tezenska Dobrava -  Smer: Tezenska dobrava - AP Mlinska</option>
                 <option value="1.Tezenska Dobrava - obračališče">- Tezenska Dobrava - obračalisče</option>
                 <option value="1.KS Silvira Tomassini">- KS Silvira Tomassini</option>
                 <option value="1.Dogoška - vrtec">- Dogoška - vrtec</option>
                 <option value="1.Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="1.Ptujska - Pošta">- Ptujska - Pošta</option>
                 <option value="1.Ptujska - Autocommerce">- Ptujska - Autocommerce</option>
                 <option value="1.Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="1.Ptujska - hitra cesta">-Ptujska - hitra cesta</option>
                 <option value="1.Titova - Prol.brigad">-Titova - Prol.brigad</option>
                 <option value="1.Titova - Nasipna">-Titova - Nasipna</option>
                 <option value="1.Ljubljanska - Pariške komune">- Ljubljanska - Pariške komune</option>
                 <option value="1.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="1.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="1.City Center">- City Center</option>
                 <option value="1.AP Mlinska">- AP Mlinska </option>
                 <option value="1.Meljska - Partizanska">- Meljska - Partizanska</option>
                 <option value="1.Meljska - trgovina">- Meljska - trgovina</option>
                 
                 
               
                 <option id="bold" value="Linija 2">•LINIJA 2 Betnavska-Razvanje - Smer: Razvanje - AP Mlinska</option>
                 <option value="2.Razvanje - obračališče">- Razvanje - obračališče</option> 
                 <option value="2.Razvanje - GD">- Razvanje - GD</option>
                 <option value="2.Razvanje - vrtnarstvo">- Razvanje - vrtnarstvo</option>
                 <option value="2.Betnavski grad">- Betnavski grad</option>
                 <option value="2.Kardeljeva - Borštnikova">- Kardeljeva - Borštnikova</option>
                 <option value="2.Kardeljeva - OŠ Tabor I">- Kardeljeva - OŠ Tabor I</option>
                 <option value="2.Kardeljeva - Knafelčeva">- Kardeljeva - Knafelčeva</option>
                 <option value="2.Betnavska - Knafelčeva">- Betnavska - Knafelčeva</option>
                 <option value="2.Goriška">- Goriška</option>
                 <option value="2.Betnavska - Metelkova">- Betnavska - Metelkova</option>
                 <option value="2.Betnavska - Focheva">- Betnavska - Focheva</option>
                 <option value="2.Betnavska - Žolgarjeva">- Betnavska - Žolgarjeva</option>
                 <option value="2.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="2.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="2.City Center">- City Center</option>
                 <option value="2.AP Mlinska">- AP Mlinska </option>
                 <option value="2.Meljska - Partizanska">- Meljska - Partizanska</option>
                 <option value="2.Meljska - trgovina">- Meljska - trgovina</option>
         
                 
                 
                 <option id="bold" value="Linija 4"> •LINIJA 4 Studenci - Smer : Limbuš Marof - AP Mlinska </option>
                 <option value="4.Limbuš - Marof">- Limbuš - Marof</option>
                 <option value="4.Limbuška c. - rondo">- Limbuška c. - rondo</option>
                 <option value="4.Limbuš - pošta">- Limbuš - pošta</option>
                 <option value="4.Pekre - GD">- Pekre - GD</option>
                 <option value="4.Marles">- Marles</option>
                 <option value="4.Limbuška c. - Lesarska">- Limbuška c. - Lesarska</option>
                 <option value="4.Lesarska šola - obračališče">- Lesarska šola - obračališče</option>
                 <option value="4.Limbuška 47">- Limbuš - Marof</option>
                 <option value="4.Limbuška c. - Pekrska">- Limbuška c. - Pekrska</option>
                 <option value="4.Valvasorjeva - transformator">- Valvasorjeva - transformator</option>
                 <option value="4.Valvasorjeva - Korenčanova">- Valvasorjeva - Korenčanova</option>
                 <option value="4.ŽP Studenci">- ŽP Studenci</option>
                 <option value="4.Valvasorjeva - OŠ Maks Durjava">- Valvasorjeva - OŠ Maks Durjava</option>
                 <option value="4.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="4.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="4.City Center">- City Center</option>
                
                 
                 <option id="bold" value="Linija 6"> •LINIJA 6 Vzpenjača- Smer:   Vzpenjača obračališče - AP Mlinska</option>
                 <option value="6.Vzpenjača - obračališče">- Vzpenjača - obračališče</option>
                 <option value="6.Pohorska ul. - pošta">- Pohorska ul. - pošta</option>
                 <option value="6.Pohorska ul. -  Mlada lipa">- Pohorska ul. -  Mlada lipa</option>
                 <option value="6.Lackova - Stara lipa">- Lackova - Stara lipa</option>
                 <option value="6.Streliška - Ul. Pohorskega odreda">- Streliška - Ul. Pohorskega odreda</option>
                 <option value="6.Radvanjska - Borštnikova">- Radvanjska - Borštnikova</option>
                 <option value="6.Radvanjska - Antoličičeva">- Radvanjska - Antoličičeva</option>
                 <option value="6.Radvanjska - trgovina">- Radvanjska - trgovina</option>
                 <option value="6.Kardeljeva - igrišče">- Kardeljeva - igrišče</option>
                 <option value="6.Radvanjska - vojašnica">- Radvanjska - vojašnica</option>
                 <option value="6.Gorkega - Preradovičeva">- Gorkega - Preradovičeva</option>
                 <option value="6.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="6.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="6.City Center">- City Center</option>
                 <option value="6.AP Mlinska">- AP Mlinska</option>
                 <option value="6.Meljska - Partizanska">- Meljska -  Partizanska</option>
                 <option value="6.Meljska -  trgovina">- Meljska -  trgovina</option>
            </select>
 
        <div>
        <img src="../slike/arrow.png" alt="" id="slikapostaje"/>
          </div>
              
             <br>
             <br>
          
                 <select name="Izstopna">
     
                 <option value="Izstopna">Izstopna postaja</option>
                 
                 
                 <option id="bold" value="Linija 1">•LINIJA 1 Tezenska Dobrava -  Smer: Tezenska dobrava - AP Mlinska</option>
                 <option value="1.Tezenska Dobrava - obračališče">- Tezenska Dobrava - obračalisče</option>
                 <option value="1.KS Silvira Tomassini">- KS Silvira Tomassini</option>
                 <option value="1.Dogoška - vrtec">- Dogoška - vrtec</option>
                 <option value="1.Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="1.Ptujska - Pošta">- Ptujska - Pošta</option>
                 <option value="1.Ptujska - Autocommerce">- Ptujska - Autocommerce</option>
                 <option value="1.Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="1.Ptujska - hitra cesta">-Ptujska - hitra cesta</option>
                 <option value="1.Titova - Prol.brigad">-Titova - Prol.brigad</option>
                 <option value="1.Titova - Nasipna">-Titova - Nasipna</option>
                 <option value="1.Ljubljanska - Pariške komune">- Ljubljanska - Pariške komune</option>
                 <option value="1.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="1.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="1.City Center">- City Center</option>
                 <option value="1.AP Mlinska">- AP Mlinska </option>
                 <option value="1.Meljska - Partizanska">- Meljska - Partizanska</option>
                 <option value="1.Meljska - trgovina">- Meljska - trgovina</option>
                 
               
         
               
                 <option id="bold" value="Linija 2">•LINIJA 2 Betnavska-Razvanje - Smer: Razvanje - AP Mlinska</option>
                 <option value="2.Razvanje - obračališče">- Razvanje - obračališče</option> 
                 <option value="2.Razvanje - GD">- Razvanje - GD</option>
                 <option value="2.Razvanje - vrtnarstvo">- Razvanje - vrtnarstvo</option>
                 <option value="2.Betnavski grad">- Betnavski grad</option>
                 <option value="2.Kardeljeva - Borštnikova">- Kardeljeva - Borštnikova</option>
                 <option value="2.Kardeljeva - OŠ Tabor I">- Kardeljeva - OŠ Tabor I</option>
                 <option value="2.Kardeljeva - Knafelčeva">- Kardeljeva - Knafelčeva</option>
                 <option value="2.Betnavska - Knafelčeva">- Betnavska - Knafelčeva</option>
                 <option value="2.Goriška">- Goriška</option>
                 <option value="2.Betnavska - Metelkova">- Betnavska - Metelkova</option>
                 <option value="2.Betnavska - Focheva">- Betnavska - Focheva</option>
                 <option value="2.Betnavska - Žolgarjeva">- Betnavska - Žolgarjeva</option>
                 <option value="2.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="2.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="2.City Center">- City Center</option>
                 <option value="2.AP Mlinska">- AP Mlinska </option>
                 <option value="2.Meljska - Partizanska">- Meljska - Partizanska</option>
                 <option value="2.Meljska - trgovina">- Meljska - trgovina</option>
                 
                 
             
                 
                 
                 <option id="bold" value="Linija 4"> •LINIJA 4 Studenci - Smer : Limbuš Marof - AP Mlinska </option>
                 <option value="4.Limbuš - Marof">- Limbuš - Marof</option>
                 <option value="4.Limbuška c. - rondo">- Limbuška c. - rondo</option>
                 <option value="4.Limbuš - pošta">- Limbuš - pošta</option>
                 <option value="4.Pekre - GD">- Pekre - GD</option>
                 <option value="4.Marles">- Marles</option>
                 <option value="4.Limbuška c. - Lesarska">- Limbuška c. - Lesarska</option>
                 <option value="4.Lesarska šola - obračališče">- Lesarska šola - obračališče</option>
                 <option value="4.Limbuška 47">- Limbuš - Marof</option>
                 <option value="4.Limbuška c. - Pekrska">- Limbuška c. - Pekrska</option>
                 <option value="4.Valvasorjeva - transformator">- Valvasorjeva - transformator</option>
                 <option value="4.Valvasorjeva - Korenčanova">- Valvasorjeva - Korenčanova</option>
                 <option value="4.ŽP Studenci">- ŽP Studenci</option>
                 <option value="4.Valvasorjeva - OŠ Maks Durjava">- Valvasorjeva - OŠ Maks Durjava</option>
                 <option value="4.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="4.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="4.City Center">- City Center</option>
                 
                 
               
                 <option id="bold" value="Linija 6"> •LINIJA 6 Vzpenjača- Smer:   Vzpenjača obračališče - AP Mlinska</option>
                 <option value="6.Vzpenjača - obračališče">- Vzpenjača - obračališče</option>
                 <option value="6.Pohorska ul. - pošta">- Pohorska ul. - pošta</option>
                 <option value="6.Pohorska ul. -  Mlada lipa">- Pohorska ul. -  Mlada lipa</option>
                 <option value="6.Lackova - Stara lipa">- Lackova - Stara lipa</option>
                 <option value="6.Streliška - Ul. Pohorskega odreda">- Streliška - Ul. Pohorskega odreda</option>
                 <option value="6.Radvanjska - Borštnikova">- Radvanjska - Borštnikova</option>
                 <option value="6.Radvanjska - Antoličičeva">- Radvanjska - Antoličičeva</option>
                 <option value="6.Radvanjska - trgovina">- Radvanjska - trgovina</option>
                 <option value="6.Kardeljeva - igrišče">- Kardeljeva - igrišče</option>
                 <option value="6.Radvanjska - vojašnica">- Radvanjska - vojašnica</option>
                 <option value="6.Gorkega - Preradovičeva">- Gorkega - Preradovičeva</option>
                 <option value="6.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="6.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="6.City Center">- City Center</option>
                 <option value="6.AP Mlinska">- AP Mlinska</option>
                 <option value="6.Meljska Partizanska">- Meljska Partizanska</option>
                 <option value="6.Meljska Trgovina">- Meljska Trgovina</option>
                 
                 
               
            </select>
                  <br/><input type="submit" value="Potrdi" name="submit" id="gumb" />
      
        </form>
    
                 
             <input type="submit" value="Priljubljene Postaje" name="submit" id="gumb" />
    </body>
      </div>
    </div>
    <div id="footer">
      Copyright &copy; MMP 
          <script type="text/javascript">
             document.write ('<p><span id="date-time">', new Date().toLocaleString(), '<\/span>.<\/p>')
             if (document.getElementById) onload = function () {
	               setInterval ("document.getElementById ('date-time').firstChild.data = new Date().toLocaleString()", 50)
}
</script>
    </div>
  </div>
    </body>
</html>

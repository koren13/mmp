<link href="../CSS/style.css" rel="stylesheet" type="text/css"/>
<link href="../CSS/Index.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MMP</title>
    </head>
    <body>
        <div id="main">
    <div id="header">
      <div id="logo">
      <input type="image" style="width: 450px" style= "height: 450px" style="align: center" src="../slike/logo_mmp.jpg" alt=""/>
      </div>
      <div id="menubar">
        <ul id="menu">
          <li class="selected"><a href="index.php">Vozni redi</a></li>
          <li><a href="../HTML/cenik.html">Cenik</a></li>
          <li><a href="../HTML/mreza_linij.html">Mreža linij</a></li>
          <li><a href="../HTML/kontakt.html">Kontakt</a></li>
          
        </ul>
      </div>
    </div>
    <div id="site_content">
      <div class="sidebar">
        <h3>Obvestila so na naslednjih postajah:</h3>
        <p><a href="https://www.facebook.com/MMP-Mariborski-mestni-prevozi-1516327975359920/?fref=ts"><img src="../slike/index.png" alt="" width="42" height="42"/></a>- Facebook</p>
        
        <hr>
        <br>
        <p><a href="https://twitter.com/mestni_prevozi"><img src="../slike/twitter.png" alt="" width="42" height="42"/></a>- Twitter</p>
        
        
      </div>
      <div id="content">
          <h1>Izberi postajo:</h1>
          <form name="submitPostaj" action="Prikaz2.php" method="POST">
            <select name="Vstopna">
                <option value="">Vstopna postaja</option>
                
                 <option id="bold" value="Linija 1">•LINIJA 1 Tezenska Dobrava -  Smer: Tezenska dobrava - AP Mlinska</option>
                 <option value="1.Tezenska Dobrava - obračališče">- Tezenska Dobrava - obračalisče</option>
                 <option value="1.KS Silvira Tomassini">- KS Silvira Tomassini</option>
                 <option value="1.Dogoška - vrtec">- Dogoška - vrtec</option>
                 <option value="1.Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="1.Ptujska - Pošta">- Ptujska - Pošta</option>
                 <option value="1.Ptujska - Autocommerce">- Ptujska - Autocommerce</option>
                 <option value="1.Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="1.Ptujska - hitra cesta">-Ptujska - hitra cesta</option>
                 <option value="1.Titova - Prol.brigad">-Titova - Prol.brigad</option>
                 <option value="1.Titova - Nasipna">-Titova - Nasipna</option>
                 <option value="1.Ljubljanska - Pariške komune">- Ljubljanska - Pariške komune</option>
                 <option value="1.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="1.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="1.City Center">- City Center</option>
                 <option value="1.AP Mlinska">- AP Mlinska </option>
                 <option value="1.Meljska - Partizanska">- Meljska - Partizanska</option>
                 <option value="1.Meljska - trgovina">- Meljska - trgovina</option>
                 
                 
               
                 <option id="bold" value="Linija 2">•LINIJA 2 Betnavska-Razvanje - Smer: Razvanje - AP Mlinska</option>
                 <option value="2.Razvanje - obračališče">- Razvanje - obračališče</option> 
                 <option value="2.Razvanje - GD">- Razvanje - GD</option>
                 <option value="2.Razvanje - vrtnarstvo">- Razvanje - vrtnarstvo</option>
                 <option value="2.Betnavski grad">- Betnavski grad</option>
                 <option value="2.Kardeljeva - Borštnikova">- Kardeljeva - Borštnikova</option>
                 <option value="2.Kardeljeva - OŠ Tabor I">- Kardeljeva - OŠ Tabor I</option>
                 <option value="2.Kardeljeva - Knafelčeva">- Kardeljeva - Knafelčeva</option>
                 <option value="2.Betnavska - Knafelčeva">- Betnavska - Knafelčeva</option>
                 <option value="2.Goriška">- Goriška</option>
                 <option value="2.Betnavska - Metelkova">- Betnavska - Metelkova</option>
                 <option value="2.Betnavska - Focheva">- Betnavska - Focheva</option>
                 <option value="2.Betnavska - Žolgarjeva">- Betnavska - Žolgarjeva</option>
                 <option value="2.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="2.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="2.City Center">- City Center</option>
                 <option value="2.AP Mlinska">- AP Mlinska </option>
                 <option value="2.Meljska - Partizanska">- Meljska - Partizanska</option>
                 <option value="2.Meljska - trgovina">- Meljska - trgovina</option>
         
                 
                 
                 <option id="bold" value="Linija 4"> •LINIJA 4 Studenci - Smer : Limbuš Marof - AP Mlinska </option>
                 <option value="4.Limbuš - Marof">- Limbuš - Marof</option>
                 <option value="4.Limbuška c. - rondo">- Limbuška c. - rondo</option>
                 <option value="4.Limbuš - pošta">- Limbuš - pošta</option>
                 <option value="4.Pekre - GD">- Pekre - GD</option>
                 <option value="4.Marles">- Marles</option>
                 <option value="4.Limbuška c. - Lesarska">- Limbuška c. - Lesarska</option>
                 <option value="4.Lesarska šola - obračališče">- Lesarska šola - obračališče</option>
                 <option value="4.Limbuška 47">- Limbuš - Marof</option>
                 <option value="4.Limbuška c. - Pekrska">- Limbuška c. - Pekrska</option>
                 <option value="4.Valvasorjeva - transformator">- Valvasorjeva - transformator</option>
                 <option value="4.Valvasorjeva - Korenčanova">- Valvasorjeva - Korenčanova</option>
                 <option value="4.ŽP Studenci">- ŽP Studenci</option>
                 <option value="4.Valvasorjeva - OŠ Maks Durjava">- Valvasorjeva - OŠ Maks Durjava</option>
                 <option value="4.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="4.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="4.City Center">- City Center</option>
                
                 
                 <option id="bold" value="Linija 6"> •LINIJA 6 Vzpenjača- Smer:   Vzpenjača obračališče - AP Mlinska</option>
                 <option value="6.Vzpenjača - obračališče">- Vzpenjača - obračališče</option>
                 <option value="6.Pohorska ul. - pošta">- Pohorska ul. - pošta</option>
                 <option value="6.Pohorska ul. -  Mlada lipa">- Pohorska ul. -  Mlada lipa</option>
                 <option value="6.Lackova - Stara lipa">- Lackova - Stara lipa</option>
                 <option value="6.Streliška - Ul. Pohorskega odreda">- Streliška - Ul. Pohorskega odreda</option>
                 <option value="6.Radvanjska - Borštnikova">- Radvanjska - Borštnikova</option>
                 <option value="6.Radvanjska - Antoličičeva">- Radvanjska - Antoličičeva</option>
                 <option value="6.Radvanjska - trgovina">- Radvanjska - trgovina</option>
                 <option value="6.Kardeljeva - igrišče">- Kardeljeva - igrišče</option>
                 <option value="6.Radvanjska - vojašnica">- Radvanjska - vojašnica</option>
                 <option value="6.Gorkega - Preradovičeva">- Gorkega - Preradovičeva</option>
                 <option value="6.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="6.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="6.City Center">- City Center</option>
                 <option value="6.AP Mlinska">- AP Mlinska</option>
                 <option value="6.Meljska - Partizanska">- Meljska -  Partizanska</option>
                 <option value="6.Meljska -  trgovina">- Meljska -  trgovina</option>
                 
                 <option id="bold" value="">• LINIJA 7 Kamnica</option>
                <option value="7.Rošpoh obračališče "> - Rošpoh - obračališče </option>
                <option value="7.Rošpoh Hojnik"> - Rošpoh - Hojnik</option>
                <option value="7.Rošpoh odcep Urban"> - Rošpoh - odcep Urban</option>
                <option value="7.Cesta v Rošpoh 115"> - Cesta v Rošpoh 115</option>
                <option value="7.Cesta v Rošpoh 65"> - Cesta v Rošpoh 65</option>
                <option value="7.Kamnica trgovina"> - Kamnica - trgovina</option>
                <option value="7.Kamnica pošta"> - Kamnica - pošta</option>
                <option value="7.Kamnica šola"> - Kamnica - šola</option>
                <option value="7.Kamnica hipodrom"> - Kamnica - hipodrom</option>
                <option value="7.Vrbanska vodovod"> - Vrbanska vodovod</option>
                <option value="7.Vrbanska šola"> - Vrbanska šola</option>
                <option value="7.Gosposvetska turnerjeva"> - Gosposvetska turnerjeva</option>
                <option value="7.Gosposvetska vrbanska"> - Gosposvetska vrbanska</option>
                <option value="7.Tretja gimnazija"> - Tretja gimnazija</option>
                <option value="7.Strossmayerjeva"> - Strossmayerjeva</option>
                <option value="7.Glavni trg Židovska"> - Glavni trg Židovska</option>
                <option value="7.City Center"> - City Center</option>
                <option value="7.AP Mlinska"> - AP Mlinska</option>
                <option value="7.Meljska partizanska"> - Meljska partizanska</option>
                <option value="7.Meljska trgovina"> - Meljska trgovina</option>
                 
                <option id="bold" value="">• LINIJIA ŠT 8: AP Mlisnak - Terme fontana</option>
                <option value="8.Terme fontana"> - Terme Fontana</option>
                <option value="8.Gosposvetska rondo"> - Gosposvetska - Rondo </option>
                <option value="8.Gosposvetska turnerjeva"> - Gosposvetska - Turnerjeva</option>
                <option value="8.Gosposvetska vrbanska"> - Gosposvetska - Vrbanska </option>
                <option value="8.Tretja gimnazija"> - Tretja gimnazija </option>
                <option value="8.Orožnova"> - Orožnova</option>
                <option value="8.Slomškov trg Ul.X.oktobra"> - Slomškov trg Ul.X. oktobra </option>
                <option value="8.Gregorčičeva"> - Gregorčičeva</option>

                 
                <option id="bold" value="">• LINIJIA ŠT 9: Zrkovci - Dogoše - AP Mlinska</option>
                <option value="9.Dupleška cesta 255"> - Dupleška cesta 255</option>
                <option value="9.Svenškova ulica 40"> - Svenškova ulica 40</option>
                <option value="9.Zrkovci na gorci 65"> - Zrkvoci - Na gorci 65</option>
                <option value="9.Zrkovci na gorci 54"> - Zrkvoci - Na gorci 54</option>
                <option value="9.Zrkovci obračališče"> - Zrkvoci - obračališe</option>
                <option value="9.Zrkovci na terasi"> - Zrkvoci - Na terasi</option>
                <option value="9.Zrkovska trgovina"> - Zrkvoska trgovina</option>
                <option value="9.Zrkovska cesta"> - Zrkovska cesta</option>
                <option value="9.Zrkovska GD"> - Zrkovska GD</option>
                <option value="9.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="9.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="9.Greenwich"> - Greenwich</option>
                <option value="9.Pobreška europark"> - Pobreška - Europark</option>
                <option value="9.Magdalena"> - Magdalena</option>
                <option value="9.Glavni trg židovska"> - Glavni trg - Židovska</option>
                <option value="9.City center"> - City Center</option>


                 
                <option id="bold" value="">• LINIJIA ŠT 10: Malečnik </option>
                <option value="10.Metava duplek križišče"> - Metava - Duplek križišče</option>
                <option value="10.Novak"> - Novak</option>
                <option value="10.Trčova griček"> - Trčova - Griček</option>
                <option value="10.Trčova 31a"> - Trčova 31a</option>
                <option value="10.Malečnik trgovina"> - Malečnik trgovina</option>
                <option value="10.Malečnik odcep trčova"> - Malečnik - odcep Trčova</option>
                <option value="10.Lorber"> - Lorber</option>
                <option value="10.Kovačič"> - Kovačič</option>
                <option value="10.Meljski hrib most"> - Meljski hrib - most</option>
                <option value="10.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="10.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="10.Greenwich"> - Greenwich</option>
                <option value="10.Pobreška europark"> - Pobreška Europark</option>
                <option value="10.Magdalena"> - Magdalena</option>
                <option value="10.Glavni trg židovska"> - Glavni trg - Židovska</option>
                <option value="10.City center"> - City center</option>
                <option value="10.Meljska partizanska"> - Meljska partizanska</option>
                <option value="10.Meljska trgovina"> - Meljska trgovina</option>

                 
                 
                 <option id="bold" value="LINIJA 13 Črnogorska ">• LINIJA 13 Črnogorska</option>
                 <option value="13.Poštni center - obračališče"> -Poštni center - obračališče</option>
                 <option value="13.Tam - Nkbm"> -Tam - Nkbm</option>
                 <option value="13.Tam - črpalka"> -Tam - črpalka</option> 
                 <option value="13.Tam - Durabus"> -Tam - Durabus</option> 
                 <option value="13.Tam - vhod"> -Tam - vhod</option> 
                 <option value="13.Perhavčeva ul. - šola"> -Perhavčeva ul. - šola</option> 
                 <option value="13.Kavčičeva"> -Kavčičeva</option> 
                 <option value="13.Zagrebška - Šerbinek"> -Zagrebška - Šerbinek</option> 
                 <option value="13.Zagrebška - Kovinar"> -Zagrebška - Kovinar</option> 
                 <option value="13.ŽP Tezno"> -ŽP Tezno</option> 
                 <option value="13.Belokranjska ulica"> -Belokranjska ulica</option> 
                 <option value="13.Makedonska"> -Makedonska</option> 
                 <option value="13.Črnogorska št. 23"> -Črnogorska št. 23</option> 
                 <option value="13.Ob gozdu"> -Ob gozdu</option> 
                 <option value="13.Nasipna - Snaga"> -Nasipna - Snaga</option> 
                 <option value="13.Pobreška - Europark"> -Pobreška - Europark</option> 
                 <option value="13.Magdalena"> -Magdalena</option> 
                 <option value="13.Glavni trg - Židovska"> -Glavni trg - Židovska</option> 
                 <option value="13.City center"> -City center</option> 


 
                  <option id="bold" value="LINIJA 15 Bresternica ">• LINIJA 15 Bresternica </option>
                 <option value="15.Košaški dol"> -Košaški dol</option>
                 <option value="15.Košaki - stiskalnica"> -Košaki - stiskalnica</option>
                 <option value="15.Na prehodu"> -Na prehodu</option>
                 <option value="15.Šentiljska - veterinar"> -Šentiljska - veterinar</option> 
                 <option value="15.Šentiljska - Počehovska"> -Šentiljska - Počehovska</option> 
                 <option value="15.Šentiljska - TMI Košaki"> -Šentiljska - TMI Košaki</option> 
                 <option value="15.Šentiljska - Medved"> -Šentiljska - Medved</option>
                 <option value="15.ŽP spomenik"> -ŽP spomenik</option>
                 <option value="15.City center"> -City center</option>
                 <option value="15.Koroška - poštna"> -Koroška - poštna</option>
                 <option value="15.Koroška - trgovina"> -Koroška - trgovina</option>
                 <option value="15.Koroška vrata"> -Koroška vrata</option>
                 <option value="15.Gosposvetska - rondo"> -Gosposvetska - rondo</option>
                 <option value="15.Terme Fontana"> -Terme Fontana</option>
                 <option value="15.Kamnica krizisce"> -Kamnica krizisce</option>
                 <option value="15.Mariborski otok - HE"> -Mariborski otok - HE</option>
                 <option value="15.Čolnarna"> -Čolnarna</option>
                 <option value="15.Bresternica - odcep"> -Bresternica - odcep</option>
                 <option value="15.Bresternica - pošta"> -Bresternica - pošta</option>
                 
                           
                 
                 
                 <option id="bold" value="LINIJA 151 Gaj nad Mariborom ">• LINIJA 151 Gaj nad Mariborom </option>               
                 <option value="151.Ap mlinska"> -AP mlinska</option>
                 <option value="151.Gosposvetska - Turnerjeva"> -City Center</option>
                 <option value="151.Glavni trg - Židovska"> -Glavni trg - Židovska</option>
                 <option value="151.Koroška - poštna"> -Koroška - poštna</option>
                 <option value="151.Strossmajerjeva"> -Strossmajerjeva</option>
                 <option value="151.Tretja gimnazija"> -Tretja gimnazija</option>
                 <option value="151.Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option>
                 <option value="151.Vrbanska šola"> -Vrbanska šola</option>
                 <option value="151.Vrbanska - vodovod"> -Vrbanska - vodovod</option>
                 <option value="151.Kamnica - hipodrom"> -Kamnica - hipodrom</option>
                 <option value="151.Kamnica šola"> -Kamnica šola</option>
                 <option value="151.Kamnica krizisce"> -Kamnica krizisce</option>
                 <option value="151.Mariborski otok - HE"> -Mariborski otok - HE</option>
                 <option value="151.Čolnarna"> -Čolnarna</option>
                 <option value="151.Bresternica - obracaslisce"> -Bresternica - obracaslisce</option>
                 <option value="151.Bresternica - pošta"> -Bresternica - pošta</option>
                 <option value="151.Bresternica - odcep"> -Bresternica - odcep</option>
                 <option value="151.Šober - žunko"> -Šober - žunko</option>
                 <option value="151.Šober - dvor"> -Šober - dvor</option>
                 <option value="151.Rušnikova žaga"> -Rušnikova žaga</option>
                 <option value="151.Smolnikov Mlin"> -Smolnikov Mlin</option>
                 <option value="151.Gaj nad Mariborom - Šiker"> -Gaj nad Mariborom - Šiker</option>
                 <option value="151.Gaj nad Mariborom - Transformator"> -Gaj nad Mariborom - Transformator</option>
                 <option value="151.Gaj nad Mariborom"> -Gaj nad Mariborom</option>
                 
         <option id="bold"value="LINIJA 16 Dogose-Zg. Duplek ">• LINIJA 16 Dogose-Zg. Duplek</option>
         <option value="16.Zg.Duplek - obracalisce"> -Zg.Duplek - obracalisce</option>
         <option value="16.Dogose - obracalisce"> -Dogose - obracalisce</option>
         <option value="16.Dogose - polje"> -Dogose - polje</option>
         <option value="16.Dogose - GD"> -Dogose - GD</option>
         <option value="16.Dupleska - kanal"> -Dupleska - kanal</option>
         <option value="16.Dupleska - Jarceva"> -Dupleska - Jarceva</option>
         <option value="16.Brezje"> -Brezje</option>
         <option value="16.Dupleska - Tezenska"> -Dupleska - Tezenska</option>
         <option value="16.Cesta XIV.divizije - vrtnarstvo"> -Cesta XIV.divizije - vrtnarstvo</option>
         <option value="16.Veljka Vlahovica - trg. center"> -Veljka Vlahovica - trg. center</option>
         <option value="16.Veljka Vlahovica - S31"> -Veljka Vlahovica - S31</option>
         <option value="16.Cufarjeva - dom st. obcanov"> -cufarjeva - dom st. obcanov</option>
         <option value="16.Cufarjeva - TVD Partizan"> -cufarjeva - TVD Partizan</option>
         <option value="16.Creenwich"> -Greenwich</option>
         <option value="16.Pobreska - Europark"> -Pobreska - Europark</option>
         <option value="16.Magdalena"> -Magdalena</option>
         <option value="16.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
         <option value="16.City Center"> -City center</option>



         <option id="bold" value="LINIJA 17 Ribnisko selo - Studenci ">• LINIJA 17 Ribnisko selo - Studenci </option>
         <option value="17.Studenci - obracalisce"> -Studenci - obracalisce</option>
         <option value="17.Erjavceva"> -Erjavceva</option>
         <option value="17.Erjavceva - ZD"> -Erjavceva - ZD</option>
         <option value="17.sarhova - obracalisce"> -sarhova - obracalisce</option>
         <option value="17.sarhova - posta"> -sarhova - posta</option>
         <option value="17.Na Poljanah - Planet Tus"> -Na Poljanah - Planet Tus</option>
         <option value="17.Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
         <option value="17.Pariske Komune"> -Pariske Komune</option>
         <option value="17.Ljubljanska - Pariske komune"> -Ljubljanska - Pariske komune</option>
         <option value="17.Ljubljanska - UKC"> -Ljubljanska - UKC</option>
         <option value="17.Magdalena"> -Magdalena</option>
         <option value="17.Europark"> -Europark</option>
         <option value="17.City center"> -City center</option>
         <option value="17.AP Mlinska"> -AP Mlinska</option>
         <option value="17.Presernova - Os Polancic"> -Presernova - Os Polancic</option>
         <option value="17.Tomsicev drevored"> -Tomsicev drevored</option>
         <option value="17.Klicek"> -Klicek</option>
         <option value="17.Ribnik 1"> -Ribnik 1</option>



         <option id="bold" value="LINIJA 18 Pekre ">• LINIJA 18 Pekre</option>
         <option value="18.Pekre - trgovina"> -Pekre - trgovinae </option>
         <option value="18.Bezjakova"> -Bezjakova </option>
         <option value="18.Lesjakova 1"> -Lesjakova 1 </option>
         <option value="18.Lesjakova 2"> -Lesjakova 2 </option>
         <option value="18.Zvezna ulica"> -Zvezna ulica </option>
         <option value="18.Ob ribniku"> -Ob ribniku</option>
         <option value="18.Lackova - Mlada lipa"> -Lackova - Mlada lipa</option>
         <option value="18.Lackova - Stara lipa"> -Lackova - Stara lipa</option>
         <option value="18.Ul. Poh. odreda - Os L. Plibersek"> -Ul. Poh. odreda - Os L. Plibersek</option>
         <option value="18.Ul. Poh. odreda - Os G. siliha"> -Ul. Poh. odreda - Os G. siliha</option>
         <option value="18.Nova vas 2"> -Nova vas 2</option>
         <option value="18.Prol.brigad - vojasnica"> -Prol.brigad - vojasnica</option>
         <option value="18.Engelsova - vojasnica"> -Engelsova - vojasnica</option>
         <option value="18.Engelsova - sarhova"> -Engelsova - sarhova</option>
         <option value="18.Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
         <option value="18.Gorkega -  Preradoviceva"> -Gorkega -  Preradoviceva</option>
         <option value="18.Ljubljanska - UKC"> -Ljubljanska - UKC</option>
         <option value="18.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
         <option value="18.City center"> -City center</option>
         <option value="18.AP Mlinska"> -AP Mlinska</option>
         <option value="18.Meljska - Partizanska"> -Meljska - Partizanska</option>
         <option value="18.Meljska - trgovina"> -Meljska - trgovina</option>



         <option id="bold" value="LINIJA 19 Sarhova ">• LINIJA 19 Sarhova</option>
         <option value="19.Sarhova - obracalisce"> -Sarhova - obracalisce</option>
         <option value="19.Sarhova - posta"> -Sarhova - posta</option>
         <option value="19.Dravograjska - Poljane"> -Dravograjska - Poljane</option>
         <option value="19.Dravograjska - Sokolska"> -Dravograjska - Sokolska</option>
         <option value="19.Gosposvetska - rondo"> -Gosposvetska - rondo</option>
         <option value="19.Gosposvetska - Turnerjeva"> -Gosposvetska - Turnerjeva</option>
         <option value="19.Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option>
         <option value="19.Tretja gimnazija"> -Tretja gimnazija</option>
         <option value="19.Strossmayerjeva"> -Strossmayerjeva</option>
         <option value="19.Koroska - Postna"> -Koroska - Postna</option>
         <option value="19.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
         <option value="19.City center"> -City center</option>


         <option id="bold" value="LINIJA 20 Grusova ">• LINIJA 20 Grusova</option>
         <option value="20.Grusova-obracalisce">  -Grusova-obracalisce </option>
         <option value="20.Grusova 1"> -Grusova 1</option> 
         <option value="20.Zimica - odcep"> -Zimica - odcep</option>
         <option value="20.Metava - odcep"> -Metava - odcep</option>
         <option value="20.Knezar"> -Knezar</option>
         <option value="20.Kronaveter"> -Kronaveter</option>
         <option value="20.Ruperce"> -Ruperce</option>
         <option value="20.Nebova II"> -Nebova II</option>
         <option value="20.Nebova I"> -Nebova I</option>
         <option value="20.Celestrina"> -Celestrina</option>
         <option value="20.Metava - obracalisce"> -Metava - obracalisce</option>
         <option value="20.Metava - Duplek krizisce"> -Metava - Duplek krizisce</option>
         <option value="20.Novak"> -Novak</option>
         <option value="20.Trcova - Gricek"> -Trcova - Gricek</option>
         <option value="20.Trcova 31a"> -Trcova 31a</option>
         <option value="20.Malecnik - trgovina"> -Malecnik - trgovina</option>
         <option value="20.Malecnik - odcep Trcova"> -Malecnik - odcep Trcova</option>
         <option value="20.Lorber"> -Lorber</option>
         <option value="20.Kovacic"> -Kovacic</option>
         <option value="20.Meljski hrib - most"> -Meljski hrib - most</option>
         <option value="20.Cufarjeva - dom st. obcanov"> -cufarjeva - dom st. obcanov</option>
         <option value="20.Cufarjeva - TVD Partizan"> -cufarjeva - TVD Partizan</option>
         <option value="20.Greenwich"> -Greenwich</option>
         <option value="20.Pobreska - Europark"> -Pobreska - Europark</option>
         <option value="20.Magdalena"> -Magdalena</option>
         <option value="20.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
         <option value="20.City center"> -City center</option>
         <option value="20.Meljska - Partizanska"> -Meljska - Partizanska</option>
         <option value="20.Meljska - trgovina"> -Meljska - trgovina</option>



         <option id="bold" value="LINIJA 21 Ljubljanska-Trzaska c. - Merkur ">• LINIJA 21 Ljubljanska-Trzaska c. - Merkur</option>
         <option value="21.Trzaska cesta - Merkur">  -Trzaska cesta - Merkur </option>
         <option value="21.Trzaska cesta - Bauhaus"> -Trzaska cesta - Bauhaus</option>
         <option value="21.E'Leclerc - obracalisce"> -E'Leclerc - obracalisce</option>
         <option value="21.Trzaska cesta - Rutar"> -Trzaska cesta - Rutar</option>
         <option value="21.Trzaska cesta - Carinarnica"> -Trzaska cesta - Carinarnica</option>
         <option value="21.Trzaska cesta - Semenarna"> -Trzaska cesta - Semenarna</option>
         <option value="21.Trzaska cesta - Elko"> -Trzaska cesta - Elko</option>
         <option value="21.Ul. Eve Lovse - Lidl"> -Ul. Eve Lovse - Lidl</option>
         <option value="21.Ul. Eve Lovse - Energetika"> -Ul. Eve Lovse - Energetika</option>
         <option value="21.Ul. Eve Lovse - Betnava"> -Ul. Eve Lovse - Betnava</option>
         <option value="21.Ljubljanska 140"> -Ljubljanska 140</option>
         <option value="21.Ljubljanska - stolpnica"> -Ljubljanska - stolpnica</option>
         <option value="21.Ljubljanska - trgovski center"> -Ljubljanska - trgovski center</option>
         <option value="21.Ljubljanska - Ferkova"> -Ljubljanska - Ferkova</option>
         <option value="21.Ljubljanska - Focheva"> -Ljubljanska - Focheva</option>
         <option value="21.Ljubljanska - Pariske komune"> -Ljubljanska - Pariske komune</option>
         <option value="21.Ljubljanska - UKC"> - Ljubljanska - UKC</option>
         <option value="21.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
         <option value="21.City center"> -City center</option>
         <option value="21.AP Mlinska"> -AP Mlinska</option>
         <option value="21.Oresko nabrezje II"> -Oresko nabrezje 2</option>
         <option value="21.Oresko nabrezje II"> -Oresko nabrezje 1</option>
         <option value="21.Ul. kraljevica Marka"> -Ul. kraljevica Marka</option>                

            </select>
 
          <div>
              <a href="index.php"><img src="../slike/arrow1.png" alt="" id="arrow1"/></a>
          </div>
          
            <select name="Izstopna">
                 <option value="">Izstopna postaja</option>
                 <option id="bold" value="Linija 1">•LINIJA 1 Tezenska Dobrava -  Smer: Tezenska dobrava - AP Mlinska</option>
                 <option value="1.KS Silvira Tomassini">- KS Silvira Tomassini</option>
                 <option value="1.Dogoška - vrtec">- Dogoška - vrtec</option>
                 <option value="1.Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="1.Ptujska - Pošta">- Ptujska - Pošta</option>
                 <option value="1.Ptujska - Autocommerce">- Ptujska - Autocommerce</option>
                 <option value="1.Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="1.Ptujska - hitra cesta">-Ptujska - hitra cesta</option>
                 <option value="1.Titova - Prol.brigad">-Titova - Prol.brigad</option>
                 <option value="1.Titova - Nasipna">-Titova - Nasipna</option>
                 <option value="1.Ljubljanska - Pariške komune">- Ljubljanska - Pariške komune</option>
                 <option value="1.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="1.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="1.City Center">- City Center</option>
                 <option value="1.AP Mlinska">- AP Mlinska </option>
                 <option value="1.Meljska - Partizanska">- Meljska - Partizanska</option>
                 <option value="1.Meljska - trgovina">- Meljska - trgovina</option>
                      
               
                 <option id="bold" value="Linija 2">•LINIJA 2 Betnavska-Razvanje - Smer: Razvanje - AP Mlinska</option>
                 <option value="2.Razvanje - GD">- Razvanje - GD</option>
                 <option value="2.Razvanje - vrtnarstvo">- Razvanje - vrtnarstvo</option>
                 <option value="2.Betnavski grad">- Betnavski grad</option>
                 <option value="2.Kardeljeva - Borštnikova">- Kardeljeva - Borštnikova</option>
                 <option value="2.Kardeljeva - OŠ Tabor I">- Kardeljeva - OŠ Tabor I</option>
                 <option value="2.Kardeljeva - Knafelčeva">- Kardeljeva - Knafelčeva</option>
                 <option value="2.Betnavska - Knafelčeva">- Betnavska - Knafelčeva</option>
                 <option value="2.Goriška">- Goriška</option>
                 <option value="2.Betnavska - Metelkova">- Betnavska - Metelkova</option>
                 <option value="2.Betnavska - Focheva">- Betnavska - Focheva</option>
                 <option value="2.Betnavska - Žolgarjeva">- Betnavska - Žolgarjeva</option>
                 <option value="2.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="2.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="2.City Center">- City Center</option>
                 <option value="2.AP Mlinska">- AP Mlinska </option>
                 <option value="2.Meljska - Partizanska">- Meljska - Partizanska</option>
                 <option value="2.Meljska - trgovina">- Meljska - trgovina</option>
                             
                 
                 <option id="bold" value="Linija 4"> •LINIJA 4 Studenci - Smer : Limbuš Marof - AP Mlinska </option>
                 <option value="4.Limbuška c. - rondo">- Limbuška c. - rondo</option>
                 <option value="4.Limbuš - pošta">- Limbuš - pošta</option>
                 <option value="4.Pekre - GD">- Pekre - GD</option>
                 <option value="4.Marles">- Marles</option>
                 <option value="4.Limbuška c. - Lesarska">- Limbuška c. - Lesarska</option>
                 <option value="4.Lesarska šola - obračališče">- Lesarska šola - obračališče</option>
                 <option value="4.Limbuška 47">- Limbuš - Marof</option>
                 <option value="4.Limbuška c. - Pekrska">- Limbuška c. - Pekrska</option>
                 <option value="4.Valvasorjeva - transformator">- Valvasorjeva - transformator</option>
                 <option value="4.Valvasorjeva - Korenčanova">- Valvasorjeva - Korenčanova</option>
                 <option value="4.ŽP Studenci">- ŽP Studenci</option>
                 <option value="4.Valvasorjeva - OŠ Maks Durjava">- Valvasorjeva - OŠ Maks Durjava</option>
                 <option value="4.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="4.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="4.City Center">- City Center</option>
                 
                 
               
                 <option id="bold" value="Linija 6"> •LINIJA 6 Vzpenjača- Smer:   Vzpenjača obračališče - AP Mlinska</option>
                 <option value="6.Pohorska ul. - pošta">- Pohorska ul. - pošta</option>
                 <option value="6.Pohorska ul. -  Mlada lipa">- Pohorska ul. -  Mlada lipa</option>
                 <option value="6.Lackova - Stara lipa">- Lackova - Stara lipa</option>
                 <option value="6.Streliška - Ul. Pohorskega odreda">- Streliška - Ul. Pohorskega odreda</option>
                 <option value="6.Radvanjska - Borštnikova">- Radvanjska - Borštnikova</option>
                 <option value="6.Radvanjska - Antoličičeva">- Radvanjska - Antoličičeva</option>
                 <option value="6.Radvanjska - trgovina">- Radvanjska - trgovina</option>
                 <option value="6.Kardeljeva - igrišče">- Kardeljeva - igrišče</option>
                 <option value="6.Radvanjska - vojašnica">- Radvanjska - vojašnica</option>
                 <option value="6.Gorkega - Preradovičeva">- Gorkega - Preradovičeva</option>
                 <option value="6.Ljubljanska - UKC">- Ljubljanska - UKC</option>
                 <option value="6.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="6.City Center">- City Center</option>
                 <option value="6.AP Mlinska">- AP Mlinska</option>
                 <option value="6.Meljska Partizanska">- Meljska Partizanska</option>
                 <option value="6.Meljska Trgovina">- Meljska Trgovina</option>
                 
                 <option id="bold" value="">• LINIJA 7 Kamnica</option>
                <option value="Rošpoh Hojnik"> - Rošpoh - Hojnik</option>
                <option value="Rošpoh odcep Urban"> - Rošpoh - odcep Urban</option>
                <option value="Cesta v Rošpoh 115"> - Cesta v Rošpoh 115</option>
                <option value="Cesta v Rošpoh 65"> - Cesta v Rošpoh 65</option>
                <option value="Kamnica trgovina"> - Kamnica - trgovina</option>
                <option value="Kamnica pošta"> - Kamnica - pošta</option>
                <option value="Kamnica šola"> - Kamnica - šola</option>
                <option value="Kamnica hipodrom"> - Kamnica - hipodrom</option>
                <option value="Vrbanska vodovod"> - Vrbanska vodovod</option>
                <option value="Vrbanska šola"> - Vrbanska šola</option>
                <option value="Gosposvetska turnerjeva"> - Gosposvetska turnerjeva</option>
                <option value="Gosposvetska vrbanska"> - Gosposvetska vrbanska</option>
                <option value="Tretja gimnazija"> - Tretja gimnazija</option>
                <option value="Strossmayerjeva"> - Strossmayerjeva</option>
                <option value="Glavni trg Židovska"> - Glavni trg Židovska</option>
                <option value="City Center"> - City Center</option>
                <option value="AP Mlinska"> - AP Mlinska</option>
                <option value="Meljska partizanska"> - Meljska partizanska</option>
                <option value="Meljska trgovina"> - Meljska trgovina</option>
                 
                 
                 <option id="bold" value="">• LINIJIA ŠT 8: Terme Fontana - AP Mlinska</option>
                <option value="8.Gosposvetska rondo"> - Gosposvetska - Rondo </option>
                <option value="8.Gosposvetska turnerjeva"> - Gosposvetska - Turnerjeva</option>
                <option value="8.Gosposvetska vrbanska"> - Gosposvetska - Vrbanska </option>
                <option value="8.Tretja gimnazija"> - Tretja gimnazija </option>
                <option value="8.Orožnova"> - Orožnova</option>
                <option value="8.Slomškov trg Ul.X.oktobra"> - Slomškov trg Ul.X. oktobra </option>
                <option value="8.Gregorčičeva"> - Gregorčičeva</option>
                <option value="8.AP Mlinska"> - AP Mlinska </option>
                 
                 
                 <option id="bold" value="">• LINIJIA ŠT 9: Zrkovci - Dogoše - AP Mlinska</option>
                <option value="9.Svenškova ulica 40"> - Svenškova ulica 40</option>
                <option value="9.Zrkovci na gorci 65"> - Zrkvoci - Na gorci 65</option>
                <option value="9.Zrkovci na gorci 54"> - Zrkvoci - Na gorci 54</option>
                <option value="9.Zrkovci obračališče"> - Zrkvoci - obračališe</option>
                <option value="9.Zrkovci na terasi"> - Zrkvoci - Na terasi</option>
                <option value="9.Zrkovska trgovina"> - Zrkvoska trgovina</option>
                <option value="9.Zrkovska cesta"> - Zrkovska cesta</option>
                <option value="9.Zrkovska GD"> - Zrkovska GD</option>
                <option value="9.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="9.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="9.Greenwich"> - Greenwich</option>
                <option value="9.Pobreška europark"> - Pobreška - Europark</option>
                <option value="9.Magdalena"> - Magdalena</option>
                <option value="9.Glavni trg židovska"> - Glavni trg - Židovska</option>
                <option value="9.Citiy center"> - Citiy Center</option>
                <option value="9.AP Mlinska"> - AP Mlinska</option>
                 
                 
                <option id="bold" value="">• LINIJIA ŠT 10: Malečnik </option>
                <option value="10.Novak"> - Novak</option>
                <option value="10.Trčova griček"> - Trčova - Griček</option>
                <option value="10.Trčova 31a"> - Trčova 31a</option>
                <option value="10.Malečnik trgovina"> - Malečnik trgovina</option>
                <option value="10.Malečnik odcep trčova"> - Malečnik - odcep Trčova</option>
                <option value="10.Lorber"> - Lorber</option>
                <option value="10.Kovačič"> - Kovačič</option>
                <option value="10.Meljski hrib most"> - Meljski hrib - most</option>
                <option value="10.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="10.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="10.Greenwich"> - Greenwich</option>
                <option value="10.Pobreška europark"> - Pobreška Europark</option>
                <option value="10.Magdalena"> - Magdalena</option>
                <option value="10.Glavni trg židovska"> - Glavni trg - Židovska</option>
                <option value="10.City center"> - City center</option>
                <option value="10.Meljska partizanska"> - Meljska partizanska</option>
                <option value="10.Meljska trgovina"> - Meljska trgovina</option>
                <option value="10.Melje obračališče"> - Melje - obračališče</option>
              
                 
                 <option id="bold" value="LINIJA 13 Črnogorska ">• LINIJA 13 Črnogorska</option>
                 <option value="13.City center"> -City center</option> 
                 <option value="13.Glavni trg - Židovska"> -Glavni trg - Židovska</option> 
                 <option value="13.Magdalena"> -Magdalena</option>
                 <option value="13.Pobreška - Europark"> -Pobreška - Europark</option>
                 <option value="13.Nasipna - Snaga"> -Nasipna - Snaga</option>
                 <option value="13.Ob gozdu"> -Ob gozdu</option>
                 <option value="13.Črnogorska št. 23"> -Črnogorska št. 23</option>
                 <option value="13.Makedonska"> -Makedonska</option>
                 <option value="13.Belokranjska ulica"> -Belokranjska ulica</option>
                 <option value="13.ŽP Tezno"> -ŽP Tezno</option>
                 <option value="13.Zagrebška - Kovinar"> -Zagrebška - Kovinar</option>
                 <option value="13.Zagrebška - Šerbinek"> -Zagrebška - Šerbinek</option>
                 <option value="13.Kavčičeva"> -Kavčičeva</option>
                 <option value="13.Perhavčeva ul. - šola"> -Perhavčeva ul. - šola</option>
                 <option value="13.Tam - vhod"> -Tam - vhod</option>
                 <option value="13.Tam - Durabus"> -Tam - Durabus</option>
                 <option value="13.Tam - črpalka"> -Tam - črpalka</option>
                 <option value="13.Tam - Nkbm"> -Tam - Nkbm</option>
                 <option value="13.Poštni center - obračališče"> -Poštni center - obračališče</option> 
                 
              
                <option id="bold" value="LINIJA 15 Bresternica ">• LINIJA 15 Bresternica </option>
                 <option value="15.Košaki - stiskalnica"> -Košaki - stiskalnica</option>
                 <option value="15.Na prehodu"> -Na prehodu</option>
                 <option value="15.Šentiljska - veterinar"> -Šentiljska - veterinar</option> 
                 <option value="15.Šentiljska - Počehovska"> -Šentiljska - Počehovska</option> 
                 <option value="15.Šentiljska - TMI Košaki"> -Šentiljska - TMI Košaki</option> 
                 <option value="15.Šentiljska - Medved"> -Šentiljska - Medved</option>
                 <option value="15.ŽP spomenik"> -ŽP spomenik</option>
                 <option value="15.City center"> -City center</option>
                 <option value="15.Koroška - poštna"> -Koroška - poštna</option>
                 <option value="15.Koroška - trgovina"> -Koroška - trgovina</option>
                 <option value="15.Koroška vrata"> -Koroška vrata</option>
                 <option value="15.Gosposvetska - rondo"> -Gosposvetska - rondo</option>
                 <option value="15.Terme Fontana"> -Terme Fontana</option>
                 <option value="15.Kamnica krizisce"> -Kamnica krizisce</option>
                 <option value="15.Mariborski otok - HE"> -Mariborski otok - HE</option>
                 <option value="15.Čolnarna"> -Čolnarna</option>
                 <option value="15.Bresternica - odcep"> -Bresternica - odcep</option>
                 <option value="15.Bresternica - pošta"> -Bresternica - pošta</option>
                 <option value="15.Bresternica - obracalisce"> -Bresternica - obracalisce</option>
               
                 
                 <option id="bold" value="LINIJA 151 Gaj nad Mariborom ">• LINIJA 151 Gaj nad Mariborom </option>               
                 <option value="151.Gosposvetska - Turnerjeva"> -City Center</option>
                 <option value="151.Glavni trg - Židovska"> -Glavni trg - Židovska</option>
                 <option value="151.Koroška - poštna"> -Koroška - poštna</option>
                 <option value="151.Strossmajerjeva"> -Strossmajerjeva</option>
                 <option value="151.Tretja gimnazija"> -Tretja gimnazija</option>
                 <option value="151.Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option>
                 <option value="151.Vrbanska šola"> -Vrbanska šola</option>
                 <option value="151.Vrbanska - vodovod"> -Vrbanska - vodovod</option>
                 <option value="151.Kamnica - hipodrom"> -Kamnica - hipodrom</option>
                 <option value="151.Kamnica šola"> -Kamnica šola</option>
                 <option value="151.Kamnica krizisce"> -Kamnica krizisce</option>
                 <option value="151.Mariborski otok - HE"> -Mariborski otok - HE</option>
                 <option value="151.Čolnarna"> -Čolnarna</option>
                 <option value="151.Bresternica - obracaslisce"> -Bresternica - obracaslisce</option>
                 <option value="151.Bresternica - pošta"> -Bresternica - pošta</option>
                 <option value="151.Bresternica - odcep"> -Bresternica - odcep</option>
                 <option value="151.Šober - žunko"> -Šober - žunko</option>
                 <option value="151.Šober - dvor"> -Šober - dvor</option>
                 <option value="151.Rušnikova žaga"> -Rušnikova žaga</option>
                 <option value="151.Smolnikov Mlin"> -Smolnikov Mlin</option>
                 <option value="151.Gaj nad Mariborom - Šiker"> -Gaj nad Mariborom - Šiker</option>
                 <option value="151.Gaj nad Mariborom - Transformator"> -Gaj nad Mariborom - Transformator</option>
                 <option value="151.Gaj nad Mariborom"> -Gaj nad Mariborom</option>
                 
                 
         <option id="bold"value="LINIJA 16 Dogose-Zg. Duplek ">• LINIJA 16 Dogose-Zg. Duplek</option>
         <option value="16.Dogose - obracalisce"> -Dogose - obracalisce</option>
         <option value="16.Dogose - polje"> -Dogose - polje</option>
         <option value="16.Dogose - GD"> -Dogose - GD</option>
         <option value="16.Dupleska - kanal"> -Dupleska - kanal</option>
         <option value="16.Dupleska - Jarceva"> -Dupleska - Jarceva</option>
         <option value="16.Brezje"> -Brezje</option>
         <option value="16.Dupleska - Tezenska"> -Dupleska - Tezenska</option>
         <option value="16.Cesta XIV.divizije - vrtnarstvo"> -Cesta XIV.divizije - vrtnarstvo</option>
         <option value="16.Veljka Vlahovica - trg. center"> -Veljka Vlahovica - trg. center</option>
         <option value="16.Veljka Vlahovica - S31"> -Veljka Vlahovica - S31</option>
         <option value="16.Cufarjeva - dom st. obcanov"> -cufarjeva - dom st. obcanov</option>
         <option value="16.Cufarjeva - TVD Partizan"> -cufarjeva - TVD Partizan</option>
         <option value="16.Creenwich"> -Greenwich</option>
         <option value="16.Pobreska - Europark"> -Pobreska - Europark</option>
         <option value="16.Magdalena"> -Magdalena</option>
         <option value="16.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
         <option value="16.City Center"> -City center</option>
         <option value="16.AP Mlinska"> -AP Mlinska</option>


         <option id="bold" value="LINIJA 17 Ribnisko selo - Studenci ">• LINIJA 17 Ribnisko selo - Studenci </option>
         <option value="17.Erjavceva"> -Erjavceva</option>
         <option value="17.Erjavceva - ZD"> -Erjavceva - ZD</option>
         <option value="17.sarhova - obracalisce"> -sarhova - obracalisce</option>
         <option value="17.sarhova - posta"> -sarhova - posta</option>
         <option value="17.Na Poljanah - Planet Tus"> -Na Poljanah - Planet Tus</option>
         <option value="17.Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
         <option value="17.Pariske Komune"> -Pariske Komune</option>
         <option value="17.Ljubljanska - Pariske komune"> -Ljubljanska - Pariske komune</option>
         <option value="17.Ljubljanska - UKC"> -Ljubljanska - UKC</option>
         <option value="17.Magdalena"> -Magdalena</option>
         <option value="17.Europark"> -Europark</option>
         <option value="17.City center"> -City center</option>
         <option value="17.AP Mlinska"> -AP Mlinska</option>
         <option value="17.Presernova - Os Polancic"> -Presernova - Os Polancic</option>
         <option value="17.Tomsicev drevored"> -Tomsicev drevored</option>
         <option value="17.Klicek"> -Klicek</option>
         <option value="17.Ribnik 1"> -Ribnik 1</option>



         <option id="bold" value="LINIJA 18 Pekre ">• LINIJA 18 Pekre</option>
         <option value="18.Bezjakova"> -Bezjakova </option>
         <option value="18.Lesjakova 1"> -Lesjakova 1 </option>
         <option value="18.Lesjakova 2"> -Lesjakova 2 </option>
         <option value="18.Zvezna ulica"> -Zvezna ulica </option>
         <option value="18.Ob ribniku"> -Ob ribniku</option>
         <option value="18.Lackova - Mlada lipa"> -Lackova - Mlada lipa</option>
         <option value="18.Lackova - Stara lipa"> -Lackova - Stara lipa</option>
         <option value="18.Ul. Poh. odreda - Os L. Plibersek"> -Ul. Poh. odreda - Os L. Plibersek</option>
         <option value="18.Ul. Poh. odreda - Os G. siliha"> -Ul. Poh. odreda - Os G. siliha</option>
         <option value="18.Nova vas 2"> -Nova vas 2</option>
         <option value="18.Prol.brigad - vojasnica"> -Prol.brigad - vojasnica</option>
         <option value="18.Engelsova - vojasnica"> -Engelsova - vojasnica</option>
         <option value="18.Engelsova - sarhova"> -Engelsova - sarhova</option>
         <option value="18.Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
         <option value="18.Gorkega -  Preradoviceva"> -Gorkega -  Preradoviceva</option>
         <option value="18.Ljubljanska - UKC"> -Ljubljanska - UKC</option>
         <option value="18.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
         <option value="18.City center"> -City center</option>
         <option value="18.AP Mlinska"> -AP Mlinska</option>
         <option value="18.Meljska - Partizanska"> -Meljska - Partizanska</option>
         <option value="18.Meljska - trgovina"> -Meljska - trgovina</option>



         <option id="bold" value="LINIJA 19 Sarhova ">• LINIJA 19 Sarhova</option>
         <option value="19.Sarhova - posta"> -Sarhova - posta</option>
         <option value="19.Dravograjska - Poljane"> -Dravograjska - Poljane</option>
         <option value="19.Dravograjska - Sokolska"> -Dravograjska - Sokolska</option>
         <option value="19.Gosposvetska - rondo"> -Gosposvetska - rondo</option>
         <option value="19.Gosposvetska - Turnerjeva"> -Gosposvetska - Turnerjeva</option>
         <option value="19.Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option>
         <option value="19.Tretja gimnazija"> -Tretja gimnazija</option>
         <option value="19.Strossmayerjeva"> -Strossmayerjeva</option>
         <option value="19.Koroska - Postna"> -Koroska - Postna</option>
         <option value="19.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
         <option value="19.City center"> -City center</option>


         <option id="bold" value="LINIJA 20 Grusova ">• LINIJA 20 Grusova</option>
         <option value="20.Grusova 1"> -Grusova 1</option> 
         <option value="20.Zimica - odcep"> -Zimica - odcep</option>
         <option value="20.Metava - odcep"> -Metava - odcep</option>
         <option value="20.Knezar"> -Knezar</option>
         <option value="20.Kronaveter"> -Kronaveter</option>
         <option value="20.Ruperce"> -Ruperce</option>
         <option value="20.Nebova II"> -Nebova II</option>
         <option value="20.Nebova I"> -Nebova I</option>
         <option value="20.Celestrina"> -Celestrina</option>
         <option value="20.Metava - obracalisce"> -Metava - obracalisce</option>
         <option value="20.Metava - Duplek krizisce"> -Metava - Duplek krizisce</option>
         <option value="20.Novak"> -Novak</option>
         <option value="20.Trcova - Gricek"> -Trcova - Gricek</option>
         <option value="20.Trcova 31a"> -Trcova 31a</option>
         <option value="20.Malecnik - trgovina"> -Malecnik - trgovina</option>
         <option value="20.Malecnik - odcep Trcova"> -Malecnik - odcep Trcova</option>
         <option value="20.Lorber"> -Lorber</option>
         <option value="20.Kovacic"> -Kovacic</option>
         <option value="20.Meljski hrib - most"> -Meljski hrib - most</option>
         <option value="20.Cufarjeva - dom st. obcanov"> -cufarjeva - dom st. obcanov</option>
         <option value="20.Cufarjeva - TVD Partizan"> -cufarjeva - TVD Partizan</option>
         <option value="20.Greenwich"> -Greenwich</option>
         <option value="20.Pobreska - Europark"> -Pobreska - Europark</option>
         <option value="20.Magdalena"> -Magdalena</option>
         <option value="20.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
         <option value="20.City center"> -City center</option>
         <option value="20.Meljska - Partizanska"> -Meljska - Partizanska</option>
         <option value="20.Meljska - trgovina"> -Meljska - trgovina</option>
         <option value="20.Melje - obracalisce"> -Melje - obracalisce</option>


         <option id="bold" value="LINIJA 21 Ljubljanska-Trzaska c. - Merkur ">• LINIJA 21 Ljubljanska-Trzaska c. - Merkur</option>
         <option value="21.Trzaska cesta - Bauhaus"> -Trzaska cesta - Bauhaus</option>
         <option value="21.E'Leclerc - obracalisce"> -E'Leclerc - obracalisce</option>
         <option value="21.Trzaska cesta - Rutar"> -Trzaska cesta - Rutar</option>
         <option value="21.Trzaska cesta - Carinarnica"> -Trzaska cesta - Carinarnica</option>
         <option value="21.Trzaska cesta - Semenarna"> -Trzaska cesta - Semenarna</option>
         <option value="21.Trzaska cesta - Elko"> -Trzaska cesta - Elko</option>
         <option value="21.Ul. Eve Lovse - Lidl"> -Ul. Eve Lovse - Lidl</option>
         <option value="21.Ul. Eve Lovse - Energetika"> -Ul. Eve Lovse - Energetika</option>
         <option value="21.Ul. Eve Lovse - Betnava"> -Ul. Eve Lovse - Betnava</option>
         <option value="21.Ljubljanska 140"> -Ljubljanska 140</option>
         <option value="21.Ljubljanska - stolpnica"> -Ljubljanska - stolpnica</option>
         <option value="21.Ljubljanska - trgovski center"> -Ljubljanska - trgovski center</option>
         <option value="21.Ljubljanska - Ferkova"> -Ljubljanska - Ferkova</option>
         <option value="21.Ljubljanska - Focheva"> -Ljubljanska - Focheva</option>
         <option value="21.Ljubljanska - Pariske komune"> -Ljubljanska - Pariske komune</option>
         <option value="21.Ljubljanska - UKC"> - Ljubljanska - UKC</option>
         <option value="21.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
         <option value="21.City center"> -City center</option>
         <option value="21.AP Mlinska"> -AP Mlinska</option>
         <option value="21.Oresko nabrezje II"> -Oresko nabrezje 2</option>
         <option value="21.Oresko nabrezje II"> -Oresko nabrezje 1</option>
         <option value="21.Ul. kraljevica Marka"> -Ul. kraljevica Marka</option>                
         <option value="21.Melje - obracalisce"> -Melje - obracalisce</option>
            </select>
                  <br/><input type="submit" value="Potrdi" name="submit" id="gumb" />
      
        </form>   
             
    </body>
      </div>
    </div>
    <div id="footer">
      Copyright &copy; MMP 
          <script type="text/javascript">
             document.write ('<p><span id="date-time">', new Date().toLocaleString(), '<\/span>.<\/p>')
             if (document.getElementById) onload = function () {
	               setInterval ("document.getElementById ('date-time').firstChild.data = new Date().toLocaleString()", 50)
}
</script>
    </div>
  </div>
    </body>
</html>



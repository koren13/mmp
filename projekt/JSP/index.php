<meta charset="UTF-8">
<link href="../CSS/style.css" rel="stylesheet" type="text/css"/>
<link href="../CSS/Index.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MMP</title>
    </head>
    <body>
        <div id="main">
    <div id="header">
      <div id="logo">
      <input type="image" style="width: 450px" style= "height: 450px" style="align: center" src="../slike/logo_mmp.jpg" alt=""/>
      </div>
      <div id="menubar">
        <ul id="menu">
          <li class="selected"><a href="index.php">Vozni redi</a></li>
          <li><a href="../HTML/cenik.html">Cenik</a></li> 
          <li><a href="../HTML/mreza_linij.html">Mreža linij</a></li>
          <li><a href="../HTML/kontakt.html">Kontakt</a></li>
          
        </ul>
      </div>
    </div>
    <div id="site_content">
      <div class="sidebar">
        <h3>Obvestila so na teh povezavah:</h3>
        <p><a href="https://www.facebook.com/MMP-Mariborski-mestni-prevozi-1516327975359920/?fref=ts"><img src="../slike/index.png" alt="" width="42" height="42"/></a>- Facebook</p>
        
        <hr>
        <br>
        <p><a href="https://twitter.com/mestni_prevozi"><img src="../slike/twitter.png" alt="" width="42" height="42"/></a>- Twitter</p>
        
        
      </div>
      <div id="content">
          <h1>Izberi postajo:</h1>
          <form name="submitPostaj" action="prikaz.php" method="POST">
            <select name="Vstopna">
                <option value="">Vstopna postaja</option>
                
                 <option id="bold" value="Linija 1">•LINIJA 1 Tezenska Dobrava -Smer: AP Mlinska -Tezenska dobrava</option>
                 <option value="1.Melje - obračališče">- Melje - obračališče</option>
                 <option value="1.Meljska - trgovina">- Meljska - trgovina</option>
                 <option value="1.Meljska - Partizanska">- Meljska - Partizanska</option>
                 <option value="1.AP Mlinska">- AP Mlinska</option>
                 <option value="1.City Center">- City  Center</option>
                 <option value="1.Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option>
                 <option value="1.Magdalenski - Park">- Magdalenski - Park</option>
                 <option value="1.Ljubljanska - Pariške komune">- Ljubljanska - Pariške komune</option>
                 <option value="1.Titova - Nasipna">- Titova - Nasipna</option>
                 <option value="1.Ptujska - Tržaška">- Ptujska - Tržaška</option>
                 <option value="1.Ptujska - hitra cesta">- Ptujska - hitra cesta</option>
                 <option value="1.Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="1.Ptujska - Autocommerce">- Ptujska - Autocommerce</option>
                 <option value="1.Ptujska - Pošta">- Ptujska - Pošta</option>
                 <option value="1.Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="1.Dogoška - vrtec">- Dogoška - vrtec</option>
                 <option value="1.KS Silvira Tomassini">- KS Silvira Tomassini</option>
                 
                 
                 <option id="bold" value="Linija 2">•LINIJA 2 Betnavska-Razvanje - Smer:  AP Mlinska - Razvanje</option>
                 <option value="2.Melje - obračališče">- Melje - obračališče</option>
                 <option value="2.Ul. kraljeviča Marka">- Ul. kraljeviča Marka</option>
                 <option value="2.Oreško nabrežje 1">- Oreško nabrežje 1</option>
                 <option value="2.Oreško nabrežje 2">- Oreško nabrežje 2</option>
                 <option value="2.AP Mlinska">- AP Mlinska</option>
                 <option value="2.City  Center">- City  Center</option>
                 <option value="2.Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option>
                 <option value="2.Tabor">- Tabor</option>
                 <option value="2.Betnavska - Žolgarjeva">- Betnavska - Žolgarjeva</option>
                 <option value="2.Betnavska - Focheva">- Betnavska - Focheva</option>
                 <option value="2.Betnavska - Metelkova">- Betnavska - Metelkova</option>
                 <option value="2.Goriška">- Goriška</option>
                 <option value="2.Betnavska - Knafelčeva">- Betnavska - Knafelčeva</option>
                 <option value="2.Kardeljeva - Knafelčeva">- Kardeljeva - Knafelčeva</option>
                 <option value="2.Kardeljeva - OŠ Tabor I">- Kardeljeva - OŠ Tabor I</option>
                 <option value="2.Kardeljeva - Borštnikova">- Kardeljeva - Borštnikova</option>
                 <option value="2.Betnavski grad">- Betnavski grad</option>
                 <option value="2.Razvanje - vrtnarstvo">- Razvanje - vrtnarstvo</option>
                 <option value="2.Razvanje - GD">- Razvanje - GD</option>
                 
                 
                 
                 <option id="bold" value="Linija 3">•LINIJA 3 Smer: Dobrava-Tezno-Gosposvetska rondo-AP Mlinska-Dobrava</option>
                 <option value="3.Pokopališče Dobrava - obračališče">- Pokopališče Dobrava - obračališče</option>
                 <option value="3.Pokopališče Dobrava - vhod">- Pokopališče Dobrava - vhod</option>
                 <option value="3.Pokopališče Dobrava - K">- Pokopališče Dobrava - K</option>
                 <option value="3.Tezno dom star. občanov">- Tezno dom star. občanov</option>
                 <option value="3.KS Silvira Tomassini">- KS Silvira Tomassini</option>
                 <option value="3.Dogoška - vrtec">- Dogoška - vrtec</option>
                 <option value="3.Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="3.Ptujska - Pošta">- Ptujska - Pošta</option>
                 <option value="3.Ptujska - Autocommerce">- tujska - Autocommerce</option>
                 <option value="3.Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="3.Ptujska - hitra cesta">- Ptujska - hitra cesta</option>
                 <option value="3.Prol.brigad - Titova">- Prol.brigad - Titova</option>
                 <option value="3.Prol. brigad - Ljubljanska">- Prol. brigad - Ljubljanska</option>
                 <option value="3.Prol. brigad - Kardeljeva">- Prol. brigad - Kardeljeva</option>
                 <option value="3.Prol. brigad - lekarna">- Prol. brigad - lekarna</option>
                 <option value="3.Prol. brigad - vojašnica">- Prol. brigad - vojašnica</option>
                 <option value="3.Dravograjska - Qlandia">- Dravograjska - Qlandia</option>
                 <option value="3.Dravograjska - I. internacionale">- Dravograjska - I. internacionale</option>
                 <option value="3.Dravograjska - Poljane">- Dravograjska - Poljane</option>
                 <option value="3.Dravograjska - Sokolska">- Dravograjska - Sokolska</option>
                 <option value="3.Gosposvetska - rondo">- Gosposvetska - rondo</option>
                 <option value="3.Gosposvetska - Turnerjeva">- Gosposvetska - Turnerjeva</option>
                 <option value="3.Gosposvetska - Vrbanska">- Gosposvetska - Vrbanska</option>
                 <option value="3.Tretja gimnazija">- Tretja gimnazija</option>
                 <option value="3.Strossmayerjeva">- Strossmayerjeva</option>
                 <option value="3.Koroška - Poštna">- Koroška - Poštna</option>
                 <option value="3.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="3.AP Mlinska">- AP Mlinska</option>
                 <option value="3.City Center">- City Center</option>
                 <option value="3.Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option>
                 <option value="3.Magdalena">- Magdalena</option>
                 <option value="3.Pobreška - Europark">- Pobreška - Europark</option>
                 <option value="3.Greenwich">- Greenwich</option>
                 <option value="3.Osojnikova - TVD Partizan">- Osojnikova - TVD Partizan</option>
                 <option value="3.Osojnikova">- Osojnikova</option>
                 <option value="3.Pokopališče Pobrežje">- Pokopališče Pobrežje</option>
                 <option value="3.Cesta XIV divizije -  OŠ D Kobala">- Cesta XIV divizije -  OŠ D Kobala</option>
                 <option value="3.Cesta XIV.divizije - vrtnarstvo">- Cesta XIV.divizije - vrtnarstvo</option>
                 <option value="3.Dupleška - Tezenska">- Dupleška - Tezenska</option>
                 <option value="3.Brezje">- Brezje</option>
                 <option value="3.Dupleška - Jarčeva">- Dupleška - Jarčeva</option>
                 <option value="3.Dupleška - kanal">- Dupleška - kanal</option>
                
                 
              
                 
                 <option id="bold" value="Linija 4"> •LINIJA 4 Studenci - Smer : AP Mlinska - Limbuš Marof</option>
                 <option value="4.AP Mlinska">- AP Mlinska</option>
                 <option value="4.City Center">- City Center</option>
                 <option value="4.Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option>
                 <option value="4.Tabor">- Tabor</option>
                 <option value="4.Valvasorjeva - OŠ Maks Durjava">- Valvasorjeva - OŠ Maks Durjava</option>
                 <option value="4.ŽP Studenci">- ŽP Studenci</option>
                 <option value="4.Valvasorjeva - Korenčanova">- Valvasorjeva - Korenčanova</option>
                 <option value="4.Valvasorjeva - transformator">- Valvasorjeva - transformator</option>
                 <option value="4.Limbuška c. - Pekrska">- Limbuška c. - Pekrska</option>
                 <option value="4.Limbuška 47">- Limbuš - Marof</option>
                 <option value="4.Studenci - obračališče">- Studenci - obračališče</option>
                 <option value="4.Lesarska šola - obračališče">- Lesarska šola - obračališče</option>
                 <option value="4.Limbuška c. - Lesarska">- Limbuška c. - Lesarska</option>
                 <option value="4.Marles">- Marles</option>
                 <option value="4.Pekre - GD">- Pekre - GD</option>
                 <option value="4.Limbuš - pošta">- Limbuš - pošta</option>
                 <option value="4.Limbuška c. - rondo">- Limbuška c. - rondo</option>
                 
                 
                 
                 
                 <option id="bold" value="Linija 6"> •LINIJA 6 Vzpenjača- Smer:  AP Mlinska - Vzpenjača obračališče</option>
                 <option value="6.Melje - obračališče">- Melje - obračališče</option>
                 <option value="6.Meljska - trgovina">- Meljska - trgovina</option>
                 <option value="6.Meljska - Partizanska">- Meljska - Partizanska</option>
                 <option value="6.AP Mlinska">- AP Mlinska</option>
                 <option value="6.City Center">- City Center</option>
                 <option value="6.Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option>
                 <option value="6.Tabor">- Tabor</option>
                 <option value="6.Gorkega - Preradovičeva">- Gorkega - Preradovičeva</option>
                 <option value="6.Gorkega - dvorana Tabor">- Gorkega - dvorana Tabor</option>
                 <option value="6.Radvanjska - vojašnica">- Radvanjska - vojašnica</option>
                 <option value="6.Kardeljeva - Prol.brigad">- Kardeljeva - Prol.brigad</option>
                 <option value="6.Radvanjska - trgovina">-Radvanjska - trgovina</option>
                 <option value="6.Radvanjska - Antoličičeva">- Radvanjska - Antoličičeva</option>
                 <option value="6.Radvanjska - Borštnikova">- Radvanjska - Borštnikova</option>
                 <option value="6.Streliška - Ul. Pohorskega odreda">- Streliška - Ul. Pohorskega odreda</option>
                 <option value="6.Lackova - Stara lipa">- Lackova - Stara lipa</option>
                 <option value="6.Pohorska ul. - Mlada lipa">- Pohorska ul. - Mlada lipa</option>
                 <option value="6.Pohorska ul. - pošta">- Pohorska ul. - pošta</option>
                 
                 <option id="bold" value="">• LINIJA 7 Kamnica</option>
		 <option value="7.Melje obračališče"> - Melje - obračališče</option>
                 <option value="7.Meljska trgovina"> - Meljska - trgovina</option>
                 <option value="7.Meljska Partizanska"> - Meljska - Partizanska</option>
                 <option value="7.AP Mlinska"> - AP Mlinska</option>
                 <option value="7.Krekova občina"> - Krekova - občina</option>
                 <option value="7.Krekova"> - Krekova</option>
                 <option value="7.Stadion Ljudski vrt"> - Stadion - Ljudski vrt</option>
                 <option value="7.Vrbanska šola"> - Vrbanska šola</option>
                 <option value="7.Kamnica hipodrom"> - Kamnica - hipodrom</option>
                 <option value="7.Kamnica šola"> - Kamnica - šola</option>
                 <option value="7.Kamnica pošta"> - Kamnica - pošta</option>
                 <option value="7.Kamnica trgovina"> - Kamnica - trgovina</option>
                 <option value="7.Cesta v Rošpoh 65"> - Cesta v Rošpoh 65</option>
                 <option value="7.Cesta v Rošpoh 115"> - Cesta v Rošpoh 115</option>
                 <option value="7.Rošpoh odcep Urban"> - Rošpoh - odcep Urban</option>
		 <option value="7.Rošpoh Hojnik"> - Rošpoh - Hojnik</option>
		 <option value="7.Rošpoh obračališče "> - Rošpoh - obračališče </option>

                
                <option id="bold" value="">• LINIJIA ŠT 8: AP Mlisnak - Terme fontana</option>
                <option value="8.AP Mlinska"> - AP Mlinska </option>
                <option value="8.Krekova občina"> - Krekova občina</option>
                <option value="8.Gregorčičeva"> - Gregorčičeva</option>
                <option value="8.Krekova"> - Krekova </option>
                <option value="8.Tretja gimnazija"> - Tretja gimnazija </option>
                <option value="8.Gosposvetska vrbanska"> - Gosposvetska - Vrbanska </option>
                <option value="8.Gosposvetska turnerjeva"> - Gosposvetska - Turnerjeva</option>
                <option value="8.Gosposvetska rondo"> - Gosposvetska - Rondo </option>
                 
                 <option id="bold" value="">• LINIJIA ŠT 9: Zrkovci - Dogoše</option>
                <option value="9.AP Mlinska"> - AP Mlinska</option>
                <option value="9.Citiy center"> - Citiy Center</option>
                <option value="9.Kneza koclja vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                <option value="9.Magdalena"> - Magdalena</option>
                <option value="9.Pobreška europark"> - Pobreška - Europark</option>
                <option value="9.Greenwich"> - Greenwich</option>
                <option value="9.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="9.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="9.Zrkovska GD"> - Zrkovska GD</option>
                <option value="9.Zrkovska cesta"> - Zrkovska cesta</option>
                <option value="9.Zrkovska trgovina"> - Zrkvoska trgovina</option>
                <option value="9.Zrkovci na terasi"> - Zrkvoci - Na terasi</option>
                <option value="9.Zrkovci obračališče"> - Zrkvoci - obračališe</option>
                <option value="9.Zrkovci na gorci 54"> - Zrkvoci - Na gorci 54</option>
                <option value="9.Zrkovci na gorci 65"> - Zrkvoci - Na gorci 65</option>
                <option value="9.Svenškova ulica 40"> - Svenškova ulica 40</option>
                <option value="9.Dupleška cesta 239"> - Dupleška cesta 239</option>
                <option value="9.Dogoše GD"> - Dogoše - GD</option>
                <option value="9.Dupleška cesta 255"> - Dupleška cesta 255</option>
                 
                 
                 
                 
                <option id="bold" value="">• LINIJIA ŠT 10: Malečnik </option>
                <option value="10.Melje obračališče"> - Melje obračališče</option>
                <option value="10.Ul.kraljeviča marka"> - Ul.Kraljeviča Marka</option>
                <option value="10.Oreško nabrežje 1"> - Oreško nabrežje 1</option>
                <option value="10.Oreško nabrežje 2"> - Oreško nabrežje 2</option>
                <option value="10.AP mlinska"> - AP Mlinska</option>
                <option value="10.City center"> - City center</option>
                <option value="10.Kneza koclja vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                <option value="10.Magdalena"> - Magdalena</option>
                <option value="10.Pobreška europark"> - Pobreška Europark</option>
                <option value="10.Greenwich"> - Greenwich</option>
                <option value="10.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="10.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="10.Meljski hrib most"> - Meljski hrib - most</option>
                <option value="10.Kovačič"> - Kovačič</option>
                <option value="10.Lorber"> - Lorber</option>
                <option value="10.Malečnik odcep trčova"> - Malečnik - odcep Trčova</option>
                <option value="10.Malečnik trgovina"> - Malečnik trgovina</option>
                <option value="10.Trčova 31a"> - Trčova 31a</option>
                <option value="10.Trčova griček"> - Trčova - Griček</option>
                <option value="10.Novak"> - Novak</option>
                <option value="10.Metava duplek križišče"> - Metava - Duplek križišče</option>

                 
                 <option id="bold" value="LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava ">• LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava</option>
                 <option value="12.Pokopališče Dobrava - obračališče"> -Pokopališče Dobrava - obračališče</option>
                 <option value="12.Dupleška - kanal "> -Dupleška - kanal </option>
                 <option value="12.Dupleška - Jarčeva"> -Dupleška - Jarčeva</option>
                 <option value="12.Brezje"> -Brezje</option>
                 <option value="12.Dupleška - Tezenska"> -Dupleška - Tezenska</option> 
                 <option value="12.Cesta XIV.divizije - vrtnarstvo"> -Cesta XIV.divizije - vrtnarstvo</option> 
                 <option value="12.Cesta XIV.divizije -  OŠ D.Kobala"> -Cesta XIV.divizije -  OŠ D.Kobala</option> 
                 <option value="12.Pokopališče Pobrežje"> -Pokopališče Pobrežje</option> 
                 <option value="12.Osojnikova"> -Osojnikova</option> 
                 <option value="12.Osojnikova - TVD Partizan"> -Osojnikova - TVD Partizan</option> 
                 <option value="12.Greenwich"> -Greenwich</option> 
                 <option value="12.Pobreška - Europark"> -Pobreška - Europark</option> 
                 <option value="12.Magdalena"> -Magdalena</option> 
                 <option value="12.Glavni trg - Židovska"> -Glavni trg - Židovska</option> 
                 <option value="12.AP Mlinska"> -AP Mlinska</option> 
                 <option value="12.City center"> -City center</option> 
                 <option value="12.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option> 
                 <option value="12.Koroška - Poštna"> -Koroška - Poštna</option> 
                 <option value="12.Strossmayerjeva"> -Strossmayerjeva</option> 
                 <option value="12.Tretja gimnazija"> -Tretja gimnazija</option> 
                 <option value="12.Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option> 
                 <option value="12.Gosposvetska - Turnerjeva"> -Gosposvetska - Turnerjeva</option>
                 <option value="12.Gosposvetska - rondo"> -Gosposvetska - rondo</option>
                 <option value="12.Dravograjska - Sokolska"> -Dravograjska - Sokolska</option>
                 <option value="12.Dravograjska - Poljane"> -Dravograjska - Poljane</option> 
                 <option value="12.Dravograjska - I. internacionale"> -Dravograjska - I. internacionale</option> 
                 <option value="12.Prol.brigad - rondo"> -Prol.brigad - rondo</option> 
                 <option value="12.Prol. brigad - lekarna"> -Prol. brigad - lekarna</option> 
                 <option value="12.Prol. brigad - Delo"> -Prol. brigad - Delo</option> 
                 <option value="12.Prol. brigad - Ljubljanska"> -Prol. brigad - Ljubljanska</option> 
                 <option value="12.Ptujska - Tržaška"> -Ptujska - Tržaška</option> 
                 <option value="12.Ptujska - hitra cesta"> -Ptujska - hitra cesta</option> 
                 <option value="12.Ptujska - Kovinar"> -Ptujska - Kovinar</option> 
                 <option value="12.Ptujska - Autocommerce"> -Ptujska - Autocommerce</option> 
                 <option value="12.Ptujska - Pošta"> -Ptujska - Pošta</option> 
                 <option value="12.Ptujska - TAM, Dogoška - vrtec"> -Ptujska - TAM, Dogoška - vrtec</option> 
                 <option value="12.KS Silvira Tomassini"> -KS Silvira Tomassini</option> 
                 <option value="12.Tezno dom star. občanov"> -Tezno dom star. občanov</option> 
                 <option value="12.Pokopališče Dobrava - K"> -Pokopališče Dobrava - K</option> 
                 <option value="12.Glavni trg - Židovska"> -Pokopališče Dobrava - vhod</option> 

                 
                 
                 <option id="bold" value="LINIJA 13 Črnogorska ">• LINIJA 13 Črnogorska</option>
                 <option value="13.Poštni center - obračališče"> -Poštni center - obračališče</option>
                 <option value="13.Tam - Nkbm"> -Tam - Nkbm</option>
                 <option value="13.Tam - črpalka"> -Tam - črpalka</option> 
                 <option value="13.Tam - Durabus"> -Tam - Durabus</option> 
                 <option value="13.Tam - vhod"> -Tam - vhod</option> 
                 <option value="13.Perhavčeva ul. - šola"> -Perhavčeva ul. - šola</option> 
                 <option value="13.Kavčičeva"> -Kavčičeva</option> 
                 <option value="13.Zagrebška - Šerbinek"> -Zagrebška - Šerbinek</option> 
                 <option value="13.Zagrebška - Kovinar"> -Zagrebška - Kovinar</option> 
                 <option value="13.ŽP Tezno"> -ŽP Tezno</option> 
                 <option value="13.Belokranjska ulica"> -Belokranjska ulica</option> 
                 <option value="13.Makedonska"> -Makedonska</option> 
                 <option value="13.Črnogorska št. 23"> -Črnogorska št. 23</option> 
                 <option value="13.Ob gozdu"> -Ob gozdu</option> 
                 <option value="13.Nasipna - Snaga"> -Nasipna - Snaga</option> 
                 <option value="13.Pobreška - Europark"> -Pobreška - Europark</option> 
                 <option value="13.Magdalena"> -Magdalena</option> 
                 <option value="13.Glavni trg - Židovska"> -Glavni trg - Židovska</option> 
                 <option value="13.City center"> -City center</option> 

                 
                 <option id="bold" value="LINIJA 15 Bresternica ">• LINIJA 15 Bresternica </option>
                 <option value="15.Bresternica - obracalisce"> -Bresternica - obracalisce</option>
                 <option value="15.Bresternica - pošta"> -Bresternica - pošta</option>
                 <option value="15.Bresternica - odcep"> -Bresternica - odcep</option>
                 <option value="15.Čolnarna"> -Čolnarna</option> 
                 <option value="15.Mariborski otok - HE"> -Mariborski otok - HE</option> 
                 <option value="15.Kamnica krizisce"> -Kamnica krizisce</option> 
                 <option value="15.Terme Fontana"> -Terme Fontana</option> 
                 <option value="15.Gosposvetska - rondo"> -Gosposvetska - rondo</option> 
                 <option value="15.Koroška vrata"> -Koroška vrata</option> 
                 <option value="15.Koroška - trgovina"> -Koroška - trgovina</option> 
                 <option value="15.Koroška - poštna"> -Koroška - poštna</option> 
                 <option value="15.Glavni trg - Židovska"> -Glavni trg - Židovska</option> 
                 <option value="15.City center"> -City center</option> 
                 <option value="15.ŽP zaliv"> -ŽP zaliv</option> 
                 <option value="15.Šentiljska - Medved"> -Šentiljska - Medved</option> 
                 <option value="15.Šubičeva - vrtec"> -Šubičeva - vrtec</option> 
                 <option value="15.Pobreška - Europark"> -Nadvoz</option> 
                 <option value="15.Magdalena"> -Pod vinogradi - transformator</option> 
                 <option value="15.Košaški dol - V zavoju"> -Košaški dol - V zavoju</option> 
                 
                 
                 <option id="bold" value="LINIJA 151 Gaj nad Mariborom ">• LINIJA 151 Gaj nad Mariborom </option>
                 <option value="151.Gaj nad Mariborom"> -Gaj nad Mariborom</option>
                 <option value="151.Gaj nad Mariborom - Transformator"> -Gaj nad Mariborom - Transformator</option>
                 <option value="151.Gaj nad Mariborom - Šiker"> -Gaj nad Mariborom - Šiker</option>
                 <option value="151.Smolnikov Mlin"> -Smolnikov Mlin</option>
                 <option value="151.Rušnikova žaga"> -Rušnikova žaga</option>
                 <option value="151.Šober - dvor"> -Šober - dvor</option>
                 <option value="151.Šober - žunko"> -Šober - žunko</option>
                 <option value="151.Bresternica - odcep"> -Bresternica - odcep</option>
                 <option value="151.Bresternica - pošta"> -Bresternica - pošta</option>
                 <option value="151.Bresternica - obracaslisce"> -Bresternica - obracaslisce</option>
                 <option value="151.Čolnarna"> -Čolnarna</option>
                 <option value="151.Mariborski otok - HE"> -Mariborski otok - HE</option>
                 <option value="151.Kamnica krizisce"> -Kamnica krizisce</option>
                 <option value="151.Kamnica šola"> -Kamnica šola</option>
                 <option value="151.Kamnica - hipodrom"> -Kamnica - hipodrom</option>
                 <option value="151.Vrbanska - vodovod"> -Vrbanska - vodovod</option>
                 <option value="151.Vrbanska šola"> -Vrbanska šola</option>
                 <option value="151.Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option>
                 <option value="151.Tretja gimnazija"> -Tretja gimnazija</option>
                 <option value="151.Strossmajerjeva"> -Strossmajerjeva</option>
                 <option value="151.Koroška - poštna"> -Koroška - poštna</option>
                 <option value="151.Glavni trg - Židovska"> -Glavni trg - Židovska</option>
                 <option value="151.City Center"> -City Center</option>
                 
                 
                 <option id="bold" value=" LINIJA 16 Dogose-Zg. Duplek ">•LINIJA 16 Dogose-Zg. Duplek</option>
                 <option value=" 16.AP Mlinska"> -AP Mlinska</option>
                 <option value=" 16.City Center"> -City center</option>
                 <option value=" 16.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value=" 16.Magdalena"> -Magdalena</option>
                 <option value=" 16.Pobreska - Europark"> -Pobreska - Europark</option>
                 <option value=" 16.Greenwich"> -Greenwich</option>
                 <option value=" 16.cufarjeva - TVD Partizan"> -cufarjeva - TVD Partizan</option>
                 <option value=" 16.cufarjeva - dom st. obcanov"> -cufarjeva - dom st. obcanov</option>
                 <option value=" 16.Veljka Vlahovica - S31"> -Veljka Vlahovica - S31</option>
                 <option value=" 16.Veljka Vlahovica - trg. center"> -Veljka Vlahovica - trg. center</option>
                 <option value=" 16.Cesta XIV.divizije - vrtnarstvo"> -Cesta XIV.divizije - vrtnarstvo</option>
                 <option value=" 16.Dupleska - Tezenska"> -Dupleska - Tezenska</option>
                 <option value=" 16.Brezje"> -Brezje</option>
                 <option value=" 16.Dupleska - Jarceva"> -Dupleska - Jarceva</option>
                 <option value=" 16.Dupleska - kanal"> -Dupleska - kanal</option>
                 <option value=" 16.Dogose - GD"> -Dogose - GD</option>
                 <option value=" 16.Dogose - polje"> -Dogose - polje</option>
                 
                 
                 
                 <option id="bold" value=" LINIJA 17 Ribnisko selo - Studenci ">•LINIJA 17 Ribnisko selo - Studenci </option>
                 <option value=" 17.Ribnisko selo - obracalisce">-Ribnisko selo - obracalisce</option>
                 <option value=" 17.Ribnik 1"> -Ribnik 1</option>
                 <option value=" 17.Klicek"> -Klicek</option>
                 <option value=" 17.Akvarij"> -Akvarij</option>
                 <option value=" 17.Krekova - obcina"> -Krekova - obcina</option>
                 <option value=" 17.Krekova"> -Krekova</option>
                 <option value=" 17.Strossmayerjeva"> -Strossmayerjeva</option>
                 <option value=" 17.Koroska - Postna"> -Koroska - Postna</option>
                 <option value=" 17.AP Mlinska"> -AP Mlinska</option>
                 <option value=" 17.City center"> -City center</option>
                 <option value=" 17.Magdalena"> -Magdalena</option>
                 <option value=" 17.Magdalenski park"> -Magdalenski park</option>
                 <option value=" 17.Ljubljanska - Pariske komune"> -Ljubljanska - Pariske komune</option>
                 <option value=" 17.Pariske Komune"> -Pariske Komune</option>
                 <option value=" 17.Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
                 <option value=" 17.Na Poljanah - Planet Tus"> -Na Poljanah - Planet Tus</option>
                 <option value=" 17.sarhova - posta"> -sarhova - posta</option>
                 <option value=" 17.sarhova - obracalisce"> -sarhova - obracalisce</option>
                 <option value=" 17.Erjavceva - ZD"> -Erjavceva - ZD</option>
                 <option value=" 17.Erjavceva"> -Erjavceva</option>
                 
                 
                 
                 <option id="bold" value="LINIJA 18 Pekre ">•LINIJA 18 Pekre</option>
                 <option value="18.Melje - obracalisce"> -Melje - obracalisce</option>
                 <option value="18.Meljska - trgovina"> -Meljska - trgovina</option>
                 <option value="18.Meljska - Partizanska"> -Meljska - Partizanska</option>
                 <option value="18.AP Mlinska"> -AP Mlinska</option>
                 <option value="18.City center"> -City center</option>
                 <option value="18.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value="18.Tabor"> -Tabor</option>
                 <option value="18.Gorkega -  Preradoviceva"> -Gorkega -  Preradoviceva</option>
                 <option value="18.Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
                 <option value="18.Engelsova - sarhova"> -Engelsova - sarhova</option>
                 <option value="18.Engelsova - vojasnica"> -Engelsova - vojasnica</option>
                 <option value="18.Prol.brigad - rond"> -Prol.brigad - rond</option>
                 <option value="18.Nova vas 2"> -Nova vas 2</option>
                 <option value="18.Ul. Poh. odreda - Os G. siliha"> -Ul. Poh. odreda - Os G. siliha</option>
                 <option value="18.Ul. Poh. odreda - Os L. Plibersek"> -Ul. Poh. odreda - Os L. Plibersek</option>
                 <option value="18.Lackova - Stara lipa"> -Lackova - Stara lipa</option>
                 <option value="18.Lackova - Mlada lipa"> -Lackova - Mlada lipa</option>
                 <option value="18.Lackova - Na gorco"> -Lackova - Na gorco</option>
                 <option value="18.Pekre - trgovina"> -Pekre - trgovinae </option>
                 
                 
                 <option id="bold" value="LINIJA 19 Sarhova ">•LINIJA 19 Sarhova</option>
                 <option value="19.AP Mlinska"> -AP Mlinska</option>
                 <option value="19.City center"> -City center</option>
                 <option value="19.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value="19.Koroska - Postna"> -Koroska - Postna</option>
                 <option value="19.Strossmayerjeva"> -Strossmayerjeva</option>
                 <option value="19.Tretja gimnazija"> -Tretja gimnazija</option>
                 <option value="19.Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option>
                 <option value="19.Gosposvetska - Turnerjeva"> -Gosposvetska - Turnerjeva</option>
                 <option value="19.Gosposvetska - rondo"> -Gosposvetska - rondo</option>
                 <option value="19.Dravograjska - Sokolska"> -Dravograjska - Sokolska</option>
                 <option value="19.Dravograjska - Poljane"> -Dravograjska - Poljane</option>
                 <option value="19.Dravograjska - I. internacionale"> -Dravograjska - I. internacionale</option>
                 <option value="19.Vrtnarska - Qlandia"> -Vrtnarska - Qlandia</option>
                 <option value="19.Ilichova - RTV"> -Ilichova - RTV</option>
                 <option value="19.Ilichova - Korbunova obracalisce"> -Ilichova - Korbunova obracalisce</option>
                 <option value="19.Korbunova - Kamenskova"> -Korbunova - Kamenskova</option>
                                
                 
                 
                  <option id="bold" value="LINIJA 20 Grusova ">•LINIJA 20 Grusova</option>
                 <option value="20.Melje - obracalisce"> -Melje - obracalisce</option>                 
                 <option value="20.Ul. kraljevica Marka"> -Ul. kraljevica Marka</option>
                 <option value="20.Oresko nabrezje 1"> -Oresko nabrezje 1</option>
                 <option value="20.Oresko nabrezje 2"> -Oresko nabrezje 2</option>
                 <option value="20.Meljska - trgovina"> -Meljska - trgovina</option>
                 <option value="20.Meljska - Partizanska"> -Meljska - Partizanska</option>
                 <option value="20.AP Mlinska"> -AP Mlinska</option>
                 <option value="20.City center"> -City center</option>
                 <option value="20.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value="20.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
                 <option value="20.Magdalena"> -Magdalena</option>
                 <option value="20.Pobreska - Europark"> -Pobreska - Europark</option>
                 <option value="20.Greenwich"> -Greenwich</option>
                 <option value="20.cufarjeva - TVD Partizan"> -cufarjeva - TVD Partizan</option>
                 <option value="20.cufarjeva - dom st. obcanov"> -cufarjeva - dom st. obcanov</option>
                 <option value="20.Meljski hrib - most"> -Meljski hrib - most</option>
                 <option value="20.Kovacic"> -Kovacic</option>
                 <option value="20.Lorber"> -Lorber</option>
                 <option value="20.Malecnik - odcep Trcova"> -Malecnik - odcep Trcova</option>
                 <option value="20.Malecnik - trgovina"> -Malecnik - trgovina</option>
                 <option value="20.Trcova 31a"> -Trcova 31a</option>
                 <option value="20.Trcova - Gricek"> -Trcova - Gricek</option>
                 <option value="20.Novek"> -Novek</option>
                 <option value="20.Metava - Duplek krizisce"> -Metava - Duplek krizisce</option>
                 <option value="20.Metava - obracalisce"> -Metava - obracalisce</option>
                 <option value="20.Metava - Duplek krizisce"> -Metava - Duplek krizisce</option>
                 <option value="20.Novak"> -Novak</option>
                 <option value="20.Trcova - Gricek"> -Trcova - Gricek</option>
                 <option value="20.Trcova 31a"> -Trcova 31a</option>
                 <option value="20.Malecnik - trgovina"> -Malecnik - trgovina</option>
                 <option value="20.Malecnik - odcep Trcova"> -Malecnik - odcep Trcova</option>
                 <option value="20.Celestrina"> -Celestrina</option>
                 <option value="20.Nebova I"> -Nebova I</option>
                 <option value="20.Nebova II"> -Nebova II</option>
                 <option value="20.Ruperce"> -Ruperce</option>
                 <option value="20.Kronaveter"> -Kronaveter</option>
                 <option value="20.Knezar"> -Knezar</option>
                 <option value="20.Metava - odcep"> -Metava - odcep</option>
                 <option value="20.Zimica - odcep"> -Zimica - odcep</option>
                 
                 
                 
                
                 
                 <option id="bold"value="LINIJA 21 Ljubljanska-Trzaska c. - Merkur ">•LINIJA 21 Ljubljanska-Trzaska c. - Merkur</option>
                 <option value="21.Melje - obracalicse"> -Melje - obracalicse</option>                 
                 <option value="21.Ul. kraljevica Marka"> -Ul. kraljevica Marka</option>
                 <option value="21.Oresko nabrezje 1"> -Oresko nabrezje 1</option>
                 <option value="21.Oresko nabrezje 2"> -Oresko nabrezje 2</option>
                 <option value="21.AP Mlinska"> -AP Mlinska</option>
                 <option value="21.City center"> -City center</option>
                 <option value="21.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value="21.Magdalenski park"> -Magdalenski park</option>
                 <option value="21.Ljubljanska - Pariske komune"> -Ljubljanska - Pariske komune</option>
                 <option value="21.Ljubljanska - Focheva"> -Ljubljanska - Focheva</option>
                 <option value="21.Ljubljanska - trgovski center"> -Ljubljanska - trgovski center</option>
                 <option value="21.Ljubljanska - stolpnica"> -Ljubljanska - stolpnica</option>
                 <option value="21.Ljubljanska 140"> -Ljubljanska 140</option>
                 <option value="21.Ul. Eve Lovse - Betnava"> -Ul. Eve Lovse - Betnava</option>
                 <option value="21.Ul. Eve Lovse - Energetika"> -Ul. Eve Lovse - Energetika</option>
                 <option value="21.Ul. Eve Lovse - Lidl"> -Ul. Eve Lovse - Lidl</option>
                 <option value="21.Trzaska cesta - Elko"> -Trzaska cesta - Elko</option>
                 <option value="21.Trzaska cesta - Semenarna"> -Trzaska cesta - Semenarna</option>
                 <option value="21.Trzaska cesta - Carinarnica"> -Trzaska cesta - Carinarnica</option>
                 <option value="21.Trzaska cesta - Rutar"> -Trzaska cesta - Rutar</option>
                 <option value="21.E'Leclerc - obracalisce"> -E'Leclerc - obracalisce</option>
                 <option value="21.Trzaska cesta - Bauhaus"> -Trzaska cesta - Bauhaus</option>
                 
            </select>
 
          <div>
              <a href="index2.php"><img src="../slike/arrow1.png" alt="" id="arrow1"/></a>
          </div>
          
            <select name="Izstopna">
                 <option value="">Izstopna postaja</option>
                 <option id="bold" value="Linija 1">•LINIJA 1 Tezenska Dobrava -Smer: AP Mlinska -Tezenska dobrava</option>
                 <option value="1.Meljska - trgovina">- Meljska - trgovina</option>
                 <option value="1.Meljska - Partizanska">- Meljska - Partizanska</option>
                 <option value="1.AP Mlinska">- AP Mlinska</option>
                 <option value="1.City Center">- City Center</option>
                 <option value="1.Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option>
                 <option value="1.Magdalenski - Park">- Magdalenski - Park</option>
                 <option value="1.Ljubljanska - Pariške komune">- Ljubljanska - Pariške komune</option>
                 <option value="1.Titova - Nasipna">- Titova - Nasipna</option>
                 <option value="1.Ptujska - Tržaška">- Ptujska - Tržaška</option>
                 <option value="1.Ptujska - hitra cesta">- Ptujska - hitra cesta</option>
                 <option value="1.Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="1.Ptujska - Autocommerce">- Ptujska - Autocommerce</option>
                 <option value="1.Ptujska - Pošta">- Ptujska - Pošta</option>
                 <option value="1.Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="1.Dogoška - vrtec">- Dogoška - vrtec</option>
                 <option value="1.KS Silvira Tomassini">- KS Silvira Tomassini</option>
                
                 
                 <option id="bold" value="Linija 2">•LINIJA 2 Betnavska-Razvanje - Smer:  AP Mlinska - Razvanje</option>
                 <option value="2.Ul. kraljeviča Marka">- Ul. kraljeviča Marka</option>
                 <option value="2.Oreško nabrežje 1">- Oreško nabrežje 1</option>
                 <option value="2.Oreško nabrežje 2">- Oreško nabrežje 2</option>
                 <option value="2.AP Mlinska">- AP Mlinska</option>
                 <option value="2.City Center">- City Center</option>
                 <option value="2.Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option>
                 <option value="2.Tabor">- Tabor</option>
                 <option value="2.Betnavska - Žolgarjeva">- Betnavska - Žolgarjeva</option>
                 <option value="2.Betnavska - Focheva">- Betnavska - Focheva</option>
                 <option value="2.Betnavska - Metelkova">- Betnavska - Metelkova</option>
                 <option value="2.Goriška">- Goriška</option>
                 <option value="2.Betnavska - Knafelčeva">- Betnavska - Knafelčeva</option>
                 <option value="2.Kardeljeva - Knafelčeva">- Kardeljeva - Knafelčeva</option>
                 <option value="2.Kardeljeva - OŠ Tabor I">- Kardeljeva - OŠ Tabor I</option>
                 <option value="2.Kardeljeva - Borštnikova">- Kardeljeva - Borštnikova</option>
                 <option value="2.Betnavski grad">- Betnavski grad</option>
                 <option value="2.Razvanje - vrtnarstvo">- Razvanje - vrtnarstvo</option>
                 <option value="2.Razvanje - GD">- Razvanje - GD</option>
             
                 
                 <option id="bold" value="Linija 3">•LINIJA 3 Smer: Dobrava-Tezno-Gosposvetska rondo-AP Mlinska-Dobrava</option>
                 <option value="3.Pokopališče Dobrava - vhod">- Pokopališče Dobrava - vhod</option>
                 <option value="3.Pokopališče Dobrava - K">- Pokopališče Dobrava - K</option>
                 <option value="3.Tezno dom star. občanov">- Tezno dom star. občanov</option>
                 <option value="3.KS Silvira Tomassini">- KS Silvira Tomassini</option>
                 <option value="3.Dogoška - vrtec">- Dogoška - vrtec</option>
                 <option value="3.Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="3.Ptujska - Pošta">- Ptujska - Pošta</option>
                 <option value="3.Ptujska - Autocommerce">- tujska - Autocommerce</option>
                 <option value="3.Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="3.Ptujska - hitra cesta">- Ptujska - hitra cesta</option>
                 <option value="3.Prol.brigad - Titova">- Prol.brigad - Titova</option>
                 <option value="3.Prol. brigad - Ljubljanska">- Prol. brigad - Ljubljanska</option>
                 <option value="3.Prol. brigad - Kardeljeva">- Prol. brigad - Kardeljeva</option>
                 <option value="3.Prol. brigad - lekarna">- Prol. brigad - lekarna</option>
                 <option value="3.Prol. brigad - vojašnica">- Prol. brigad - vojašnica</option>
                 <option value="3.Dravograjska - Qlandia">- Dravograjska - Qlandia</option>
                 <option value="3.Dravograjska - I. internacionale">- Dravograjska - I. internacionale</option>
                 <option value="3.Dravograjska - Poljane">- Dravograjska - Poljane</option>
                 <option value="3.Dravograjska - Sokolska">- Dravograjska - Sokolska</option>
                 <option value="3.Gosposvetska - rondo">- Gosposvetska - rondo</option>
                 <option value="3.Gosposvetska - Turnerjeva">- Gosposvetska - Turnerjeva</option>
                 <option value="3.Gosposvetska - Vrbanska">- Gosposvetska - Vrbanska</option>
                 <option value="3.Tretja gimnazija">- Tretja gimnazija</option>
                 <option value="3.Strossmayerjeva">- Strossmayerjeva</option>
                 <option value="3.Koroška - Poštna">- Koroška - Poštna</option>
                 <option value="3.Glavni trg - Židovska">- Glavni trg - Židovska</option>
                 <option value="3.AP Mlinska">- AP Mlinska</option>
                 <option value="3.City Center">- City Center</option>
                 <option value="3.Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option>
                 <option value="3.Magdalena">- Magdalena</option>
                 <option value="3.Pobreška - Europark">- Pobreška - Europark</option>
                 <option value="3.Greenwich">- Greenwich</option>
                 <option value="3.Osojnikova - TVD Partizan">- Osojnikova - TVD Partizan</option>
                 <option value="3.Osojnikova">- Osojnikova</option>
                 <option value="3.Pokopališče Pobrežje">- Pokopališče Pobrežje</option>
                 <option value="3.Cesta XIV divizije -  OŠ D Kobala">- Cesta XIV divizije -  OŠ D Kobala</option>
                 <option value="3.Cesta XIV.divizije - vrtnarstvo">- Cesta XIV.divizije - vrtnarstvo</option>
                 <option value="3.Dupleška - Tezenska">- Dupleška - Tezenska</option>
                 <option value="3.Brezje">- Brezje</option>
                 <option value="3.Dupleška - Jarčeva">- Dupleška - Jarčeva</option>
                 <option value="3.Dupleška - kanal">- Dupleška - kanal</option>
                 
              
                 
                 <option id="bold" value="Linija 4"> •LINIJA 4 Studenci - Smer : AP Mlinska - Limbuš Marof</option>
                 <option value="4.City Center">- City Center</option>
                 <option value="4.Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option>
                 <option value="4.Tabor">- Tabor</option>
                 <option value="4.Valvasorjeva - OŠ Maks Durjava">- Valvasorjeva - OŠ Maks Durjava</option>
                 <option value="4.ŽP Studenci">- ŽP Studenci</option>
                 <option value="4.Valvasorjeva - Korenčanova">- Valvasorjeva - Korenčanova</option>
                 <option value="4.Valvasorjeva - transformator">- Valvasorjeva - transformator</option>
                 <option value="4.Limbuška c. - Pekrska">- Limbuška c. - Pekrska</option>
                 <option value="4.Limbuška 47">- Limbuš - Marof</option>
                 <option value="4.Studenci - obračališče">- Studenci - obračališče</option>
                 <option value="4.Lesarska šola - obračališče">- Lesarska šola - obračališče</option>
                 <option value="4.Limbuška c. - Lesarska">- Limbuška c. - Lesarska</option>
                 <option value="4.Marles">- Marles</option>
                 <option value="4.Pekre - GD">- Pekre - GD</option>
                 <option value="4.Limbuš - pošta">- Limbuš - pošta</option>
                 <option value="4.Limbuška c. - rondo">- Limbuška c. - rondo</option>
                
               
                 
                 <option id="bold" value="Linija 6"> •LINIJA 6 Vzpenjača- Smer:  AP Mlinska - Vzpenjača obračališče</option>
                 <option value="6.Meljska - trgovina">- Meljska trgovina</option>
                 <option value="6.Meljska - Partizanska">- Meljska -  Partizanska</option>
                 <option value="6.AP Mlinska">- AP Mlinska</option>
                 <option value="6.City Center">- City Center</option>
                 <option value="6.Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option>
                 <option value="6.Tabor">- Tabor</option>
                 <option value="6.Gorkega - Preradovičeva">- Gorkega - Preradovičeva</option>
                 <option value="6.Gorkega - dvorana Tabor">- Gorkega - dvorana Tabor</option>
                 <option value="6.Radvanjska - vojašnica">- Radvanjska - vojašnica</option>
                 <option value="6.Kardeljeva - Prol.brigad">- Kardeljeva - Prol.brigad</option>
                 <option value="6.Radvanjska - trgovina">-Radvanjska - trgovina</option>
                 <option value="6.Radvanjska - Antoličičeva">- Radvanjska - Antoličičeva</option>
                 <option value="6.Radvanjska - Borštnikova">- Radvanjska - Borštnikova</option>
                 <option value="6.Streliška - Ul. Pohorskega odreda">- Streliška - Ul. Pohorskega odreda</option>
                 <option value="6.Lackova - Stara lipa">- Lackova - Stara lipa</option>
                 <option value="6.Pohorska ul. -  Mlada lipa">- Pohorska ul. -  Mlada lipa</option>
                 <option value="6.Pohorska ul. -  pošta">- Pohorska ul. -  pošta</option>
                 
                    <option id="bold" value="">• LINIJA 7 Kamnica</option>
                 <option value="Meljska trgovina"> - Meljska - trgovina</option>
                 <option value="Meljska Partizanska"> - Meljska - Partizanska</option>
                 <option value="AP Mlinska"> - AP Mlinska</option>
                 <option value="Krekova občina"> - Krekova - občina</option>
                 <option value="Krekova"> - Krekova</option>
                 <option value="Stadion Ljudski vrt"> - Stadion - Ljudski vrt</option>
                 <option value="Vrbanska šola"> - Vrbanska šola</option>
                 <option value="Kamnica hipodrom"> - Kamnica - hipodrom</option>
                 <option value="Kamnica šola"> - Kamnica - šola</option>
                 <option value="Kamnica pošta"> - Kamnica - pošta</option>
                 <option value="Kamnica trgovina"> - Kamnica - trgovina</option>
                 <option value="Cesta v Rošpoh 65"> - Cesta v Rošpoh 65</option>
                 <option value="Cesta v Rošpoh 115"> - Cesta v Rošpoh 115</option>
                 <option value="Rošpoh odcep Urban"> - Rošpoh - odcep Urban</option>
		 <option value="Rošpoh Hojnik"> - Rošpoh - Hojnik</option>
		 <option value="Rošpoh obračališče "> - Rošpoh - obračališče </option>
                 
                 
                 <option id="bold" value="">• LINIJIA ŠT 8: AP Mliska - Terme fontana</option>
                <option value="8.Krekova občina"> - Krekova občina</option>
                <option value="8.Gregorčičeva"> - Gregorčičeva</option>
                <option value="8.Krekova"> - Krekova </option>
                <option value="8.Tretja gimnazija"> - Tretja gimnazija </option>
                <option value="8.Gosposvetska vrbanska"> - Gosposvetska - Vrbanska </option>
                <option value="8.Gosposvetska turnerjeva"> - Gosposvetska - Turnerjeva</option>
                <option value="8.Gosposvetska rondo"> - Gosposvetska - Rondo </option>
                <option value="8.Terme fontana"> - Terme Fontana</option>
                 
                 
                <option id="bold" value="">• LINIJIA ŠT 9: Zrkovci - Dogoše</option>
                <option value="9.Citiy center"> - Citiy Center</option>
                <option value="9.Kneza koclja vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                <option value="9.Magdalena"> - Magdalena</option>
                <option value="9.Pobreška europark"> - Pobreška - Europark</option>
                <option value="9.Greenwich"> - Greenwich</option>
                <option value="9.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="9.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="9.Zrkovska GD"> - Zrkovska GD</option>
                <option value="9.Zrkovska cesta"> - Zrkovska cesta</option>
                <option value="9.Zrkovska trgovina"> - Zrkvoska trgovina</option>
                <option value="9.Zrkovci na terasi"> - Zrkvoci - Na terasi</option>
                <option value="9.Zrkovci obračališče"> - Zrkvoci - obračališe</option>
                <option value="9.Zrkovci na gorci 54"> - Zrkvoci - Na gorci 54</option>
                <option value="9.Zrkovci na gorci 65"> - Zrkvoci - Na gorci 65</option>
                <option value="9.Svenškova ulica 40"> - Svenškova ulica 40</option>
                <option value="9.Dupleška cesta 239"> - Dupleška cesta 239</option>
                <option value="9.Dogoše GD"> - Dogoše - GD</option>
                <option value="9.Dupleška cesta 255"> - Dupleška cesta 255</option>
                 
                 
                  <option id="bold" value="">• LINIJIA ŠT 10: Malečnik </option>
                <option value="10.Ul.kraljeviča marka"> - Ul.Kraljeviča Marka</option>
                <option value="10.Oreško nabrežje 1"> - Oreško nabrežje 1</option>
                <option value="10.Oreško nabrežje 2"> - Oreško nabrežje 2</option>
                <option value="10.AP mlinska"> - AP Mlinska</option>
                <option value="10.City center"> - City center</option>
                <option value="10.Kneza koclja vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                <option value="10.Magdalena"> - Magdalena</option>
                <option value="10.Pobreška europark"> - Pobreška Europark</option>
                <option value="10.Greenwich"> - Greenwich</option>
                <option value="10.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="10.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="10.Meljski hrib most"> - Meljski hrib - most</option>
                <option value="10.Kovačič"> - Kovačič</option>
                <option value="10.Lorber"> - Lorber</option>
                <option value="10.Malečnik odcep trčova"> - Malečnik - odcep Trčova</option>
                <option value="10.Malečnik trgovina"> - Malečnik trgovina</option>
                <option value="10.Trčova 31a"> - Trčova 31a</option>
                <option value="10.Trčova griček"> - Trčova - Griček</option>
                <option value="10.Novak"> - Novak</option>
                <option value="10.Metava duplek križišče"> - Metava - Duplek križišče</option>
                <option value="10.Metava obračališče"> - Metava - obračališče</option>              
                 
                 <option value="LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava ">•LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava</option>
                 <option value="12.Dupleška - kanal "> -Dupleška - kanal </option>
                 <option value="12.Dupleška - Jarčeva"> -Dupleška - Jarčeva</option>
                 <option value="12.Brezje"> -Brezje</option>
                 <option value="12.Dupleška - Tezenska"> -Dupleška - Tezenska</option> 
                 <option value="12.Cesta XIV.divizije - vrtnarstvo"> -Cesta XIV.divizije - vrtnarstvo</option> 
                 <option value="12.Cesta XIV.divizije -  OŠ D.Kobala"> -Cesta XIV.divizije -  OŠ D.Kobala</option> 
                 <option value="12.Pokopališče Pobrežje"> -Pokopališče Pobrežje</option> 
                 <option value="12.Osojnikova"> -Osojnikova</option> 
                 <option value="12.Osojnikova - TVD Partizan"> -Osojnikova - TVD Partizan</option> 
                 <option value="12.Greenwich"> -Greenwich</option> 
                 <option value="12.Pobreška - Europark"> -Pobreška - Europark</option> 
                 <option value="12.Magdalena"> -Magdalena</option> 
                 <option value="12.Glavni trg - Židovska"> -Glavni trg - Židovska</option> 
                 <option value="12.AP Mlinska"> -AP Mlinska</option> 
                 <option value="12.City center"> -City center</option> 
                 <option value="12.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option> 
                 <option value="12.Koroška - Poštna"> -Koroška - Poštna</option> 
                 <option value="12.Strossmayerjeva"> -Strossmayerjeva</option> 
                 <option value="12.Tretja gimnazija"> -Tretja gimnazija</option> 
                 <option value="12.Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option> 
                 <option value="12.Gosposvetska - Turnerjeva"> -Gosposvetska - Turnerjeva</option>
                 <option value="12.Gosposvetska - rondo"> -Gosposvetska - rondo</option>
                 <option value="12.Dravograjska - Sokolska"> -Dravograjska - Sokolska</option>
                 <option value="12.Dravograjska - Poljane"> -Dravograjska - Poljane</option> 
                 <option value="12.Dravograjska - I. internacionale"> -Dravograjska - I. internacionale</option> 
                 <option value="12.Prol.brigad - rondo"> -Prol.brigad - rondo</option> 
                 <option value="12.Prol. brigad - lekarna"> -Prol. brigad - lekarna</option> 
                 <option value="12.Prol. brigad - Delo"> -Prol. brigad - Delo</option> 
                 <option value="12.Prol. brigad - Ljubljanska"> -Prol. brigad - Ljubljanska</option> 
                 <option value="12.Ptujska - Tržaška"> -Ptujska - Tržaška</option> 
                 <option value="12.Ptujska - hitra cesta"> -Ptujska - hitra cesta</option> 
                 <option value="12.Ptujska - Kovinar"> -Ptujska - Kovinar</option> 
                 <option value="12.Ptujska - Autocommerce"> -Ptujska - Autocommerce</option> 
                 <option value="12.Ptujska - Pošta"> -Ptujska - Pošta</option> 
                 <option value="12.Ptujska - TAM, Dogoška - vrtec"> -Ptujska - TAM, Dogoška - vrtec</option> 
                 <option value="12.KS Silvira Tomassini"> -KS Silvira Tomassini</option> 
                 <option value="12.Tezno dom star. občanov"> -Tezno dom star. občanov</option> 
                 <option value="12.Pokopališče Dobrava - K"> -Pokopališče Dobrava - K</option> 
                 <option value="12.Glavni trg - Židovska"> -Pokopališče Dobrava - vhod</option>
                 <option value="12.Pokopališče Dobrava - obračališče"> -Pokopališče Dobrava - obračališče</option>
                 
                 
                 
                 
                 <option id="bold" value="LINIJA 13 Črnogorska ">• LINIJA 13 Črnogorska</option>
                 <option value="13.Tam - Nkbm"> -Tam - Nkbm</option>
                 <option value="13.Tam - črpalka"> -Tam - črpalka</option> 
                 <option value="13.Tam - Durabus"> -Tam - Durabus</option> 
                 <option value="13.Tam - vhod"> -Tam - vhod</option> 
                 <option value="13.Perhavčeva ul. - šola"> -Perhavčeva ul. - šola</option> 
                 <option value="13.Kavčičeva"> -Kavčičeva</option> 
                 <option value="13.Zagrebška - Šerbinek"> -Zagrebška - Šerbinek</option> 
                 <option value="13.Zagrebška - Kovinar"> -Zagrebška - Kovinar</option> 
                 <option value="13.ŽP Tezno"> -ŽP Tezno</option> 
                 <option value="13.Belokranjska ulica"> -Belokranjska ulica</option> 
                 <option value="13.Makedonska"> -Makedonska</option> 
                 <option value="13.Črnogorska št. 23"> -Črnogorska št. 23</option> 
                 <option value="13.Ob gozdu"> -Ob gozdu</option> 
                 <option value="13.Nasipna - Snaga"> -Nasipna - Snaga</option> 
                 <option value="13.Pobreška - Europark"> -Pobreška - Europark</option> 
                 <option value="13.Magdalena"> -Magdalena</option> 
                 <option value="13.Glavni trg - Židovska"> -Glavni trg - Židovska</option> 
                 <option value="13.City center"> -City center</option> 
                 <option value="13.AP Mlinska"> -AP Mlinska</option>
                 
                 
                 <option id="bold" value="LINIJA 15 Bresternica ">• LINIJA 15 Bresternica </option>
                 <option value="15.Bresternica - pošta"> -Bresternica - pošta</option>
                 <option value="15.Bresternica - odcep"> -Bresternica - odcep</option>
                 <option value="15.Čolnarna"> -Čolnarna</option> 
                 <option value="15.Mariborski otok - HE"> -Mariborski otok - HE</option> 
                 <option value="15.Kamnica krizisce"> -Kamnica krizisce</option> 
                 <option value="15.Terme Fontana"> -Terme Fontana</option> 
                 <option value="15.Gosposvetska - rondo"> -Gosposvetska - rondo</option> 
                 <option value="15.Koroška vrata"> -Koroška vrata</option> 
                 <option value="15.Koroška - trgovina"> -Koroška - trgovina</option> 
                 <option value="15.Koroška - poštna"> -Koroška - poštna</option> 
                 <option value="15.Glavni trg - Židovska"> -Glavni trg - Židovska</option> 
                 <option value="15.City center"> -City center</option> 
                 <option value="15.ŽP zaliv"> -ŽP zaliv</option> 
                 <option value="15.Šentiljska - Medved"> -Šentiljska - Medved</option> 
                 <option value="15.Šubičeva - vrtec"> -Šubičeva - vrtec</option> 
                 <option value="15.Pobreška - Europark"> -Nadvoz</option> 
                 <option value="15.Magdalena"> -Pod vinogradi - transformator</option> 
                 <option value="15.Košaški dol - V zavoju"> -Košaški dol - V zavoju</option>
                 <option value="15.Košaški dol"> -Košaški dol</option>
                 
                 
                 <option id="bold" value="LINIJA 151 Gaj nad Mariborom ">• LINIJA 151 Gaj nad Mariborom </option>
                 <option value="151.Gaj nad Mariborom"> -Gaj nad Mariborom</option>
                 <option value="151.Gaj nad Mariborom - Transformator"> -Gaj nad Mariborom - Transformator</option>
                 <option value="151.Gaj nad Mariborom - Šiker"> -Gaj nad Mariborom - Šiker</option>
                 <option value="151.Smolnikov Mlin"> -Smolnikov Mlin</option>
                 <option value="151.Rušnikova žaga"> -Rušnikova žaga</option>
                 <option value="151.Šober - dvor"> -Šober - dvor</option>
                 <option value="151.Šober - žunko"> -Šober - žunko</option>
                 <option value="151.Bresternica - odcep"> -Bresternica - odcep</option>
                 <option value="151.Bresternica - pošta"> -Bresternica - pošta</option>
                 <option value="151.Bresternica - obracaslisce"> -Bresternica - obracaslisce</option>
                 <option value="151.Čolnarna"> -Čolnarna</option>
                 <option value="151.Mariborski otok - HE"> -Mariborski otok - HE</option>
                 <option value="151.Kamnica krizisce"> -Kamnica krizisce</option>
                 <option value="151.Kamnica šola"> -Kamnica šola</option>
                 <option value="151.Kamnica - hipodrom"> -Kamnica - hipodrom</option>
                 <option value="151.Vrbanska - vodovod"> -Vrbanska - vodovod</option>
                 <option value="151.Vrbanska šola"> -Vrbanska šola</option>
                 <option value="151.Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option>
                 <option value="151.Tretja gimnazija"> -Tretja gimnazija</option>
                 <option value="151.Strossmajerjeva"> -Strossmajerjeva</option>
                 <option value="151.Koroška - poštna"> -Koroška - poštna</option>
                 <option value="151.Glavni trg - Židovska"> -Glavni trg - Židovska</option>
                 <option value="151.City Center"> -City Center</option>
                 
                 
                <option id="bold" value=" LINIJA 16 Dogose-Zg. Duplek ">•LINIJA 16 Dogose-Zg. Duplek</option>
                 <option value=" 16.City Center"> -City center</option>
                 <option value=" 16.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value=" 16.Magdalena"> -Magdalena</option>
                 <option value=" 16.Pobreska - Europark"> -Pobreska - Europark</option>
                 <option value=" 16.Greenwich"> -Greenwich</option>
                 <option value=" 16.cufarjeva - TVD Partizan"> -cufarjeva - TVD Partizan</option>
                 <option value=" 16.cufarjeva - dom st. obcanov"> -cufarjeva - dom st. obcanov</option>
                 <option value=" 16.Veljka Vlahovica - S31"> -Veljka Vlahovica - S31</option>
                 <option value=" 16.Veljka Vlahovica - trg. center"> -Veljka Vlahovica - trg. center</option>
                 <option value=" 16.Cesta XIV.divizije - vrtnarstvo"> -Cesta XIV.divizije - vrtnarstvo</option>
                 <option value=" 16.Dupleska - Tezenska"> -Dupleska - Tezenska</option>
                 <option value=" 16.Brezje"> -Brezje</option>
                 <option value=" 16.Dupleska - Jarceva"> -Dupleska - Jarceva</option>
                 <option value=" 16.Dupleska - kanal"> -Dupleska - kanal</option>
                 <option value=" 16.Dogose - GD"> -Dogose - GD</option>
                 <option value=" 16.Dogose - polje"> -Dogose - polje</option>
                 <option value=" 16.Dogose - obracalisce"> -Dogose - obracalisce</option>
                 <option value=" 16.Zg.Duplek - obracalisce"> -Zg.Duplek - obracalisce</option>
                 
                 
                 <option id="bold" value=" LINIJA 17 Ribnisko selo - Studenci ">•LINIJA 17 Ribnisko selo - Studenci </option>
                 <option value=" 17.Ribnik 1"> -Ribnik 1</option>
                 <option value=" 17.Klicek"> -Klicek</option>
                 <option value=" 17.Akvarij"> -Akvarij</option>
                 <option value=" 17.Krekova - obcina"> -Krekova - obcina</option>
                 <option value=" 17.Krekova"> -Krekova</option>
                 <option value=" 17.Strossmayerjeva"> -Strossmayerjeva</option>
                 <option value=" 17.Koroska - Postna"> -Koroska - Postna</option>
                 <option value=" 17.AP Mlinska"> -AP Mlinska</option>
                 <option value=" 17.City center"> -City center</option>
                 <option value=" 17.Magdalena"> -Magdalena</option>
                 <option value=" 17.Magdalenski park"> -Magdalenski park</option>
                 <option value=" 17.Ljubljanska - Pariske komune"> -Ljubljanska - Pariske komune</option>
                 <option value=" 17.Pariske Komune"> -Pariske Komune</option>
                 <option value=" 17.Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
                 <option value=" 17.Na Poljanah - Planet Tus"> -Na Poljanah - Planet Tus</option>
                 <option value=" 17.sarhova - posta"> -sarhova - posta</option>
                 <option value=" 17.sarhova - obracalisce"> -sarhova - obracalisce</option>
                 <option value=" 17.Erjavceva - ZD"> -Erjavceva - ZD</option>
                 <option value=" 17.Erjavceva"> -Erjavceva</option>
                 <option value=" 17.Studenci - obracalisce"> -Studenci - obracalisce</option>
                 
                 
                 <option id="bold" value="LINIJA 18 Pekre ">•LINIJA 18 Pekre</option>
                 <option value="18.Meljska - trgovina"> -Meljska - trgovina</option>
                 <option value="18.Meljska - Partizanska"> -Meljska - Partizanska</option>
                 <option value="18.AP Mlinska"> -AP Mlinska</option>
                 <option value="18.City center"> -City center</option>
                 <option value="18.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value="18.Tabor"> -Tabor</option>
                 <option value="18.Gorkega -  Preradoviceva"> -Gorkega -  Preradoviceva</option>
                 <option value="18.Koresova - dvorana Tabor"> -Koresova - dvorana Tabor</option>
                 <option value="18.Engelsova - sarhova"> -Engelsova - sarhova</option>
                 <option value="18.Engelsova - vojasnica"> -Engelsova - vojasnica</option>
                 <option value="18.Prol.brigad - rond"> -Prol.brigad - rond</option>
                 <option value="18.Nova vas 2"> -Nova vas 2</option>
                 <option value="18.Ul. Poh. odreda - Os G. siliha"> -Ul. Poh. odreda - Os G. siliha</option>
                 <option value="18.Ul. Poh. odreda - Os L. Plibersek"> -Ul. Poh. odreda - Os L. Plibersek</option>
                 <option value="18.Lackova - Stara lipa"> -Lackova - Stara lipa</option>
                 <option value="18.Lackova - Mlada lipa"> -Lackova - Mlada lipa</option>
                 <option value="18.Lackova - Na gorco"> -Lackova - Na gorco</option>
                 <option value="18.Pekre - trgovina"> -Pekre - trgovinae </option>
                 
                 
                 <option id="bold" value="LINIJA 19 Sarhova ">•LINIJA 19 Sarhova</option>
                 <option value="19.City center"> -City center</option>
                 <option value="19.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value="19.Koroska - Postna"> -Koroska - Postna</option>
                 <option value="19.Strossmayerjeva"> -Strossmayerjeva</option>
                 <option value="19.Tretja gimnazija"> -Tretja gimnazija</option>
                 <option value="19.Gosposvetska - Vrbanska"> -Gosposvetska - Vrbanska</option>
                 <option value="19.Gosposvetska - Turnerjeva"> -Gosposvetska - Turnerjeva</option>
                 <option value="19.Gosposvetska - rondo"> -Gosposvetska - rondo</option>
                 <option value="19.Dravograjska - Sokolska"> -Dravograjska - Sokolska</option>
                 <option value="19.Dravograjska - Poljane"> -Dravograjska - Poljane</option>
                 <option value="19.Dravograjska - I. internacionale"> -Dravograjska - I. internacionale</option>
                 <option value="19.Vrtnarska - Qlandia"> -Vrtnarska - Qlandia</option>
                 <option value="19.Ilichova - RTV"> -Ilichova - RTV</option>
                 <option value="19.Ilichova - Korbunova obracalisce"> -Ilichova - Korbunova obracalisce</option>
                 <option value="19.Korbunova - Kamenskova"> -Korbunova - Kamenskova</option>
                 <option value="19.Sarhova - obracalisce"> -sarhova - obracalisce</option>               
                 
                 
                  <option id="bold" value="LINIJA 20 Grusova ">•LINIJA 20 Grusova</option>                 
                 <option value="20.Ul. kraljevica Marka"> -Ul. kraljevica Marka</option>
                 <option value="20.Oresko nabrezje 1"> -Oresko nabrezje 1</option>
                 <option value="20.Oresko nabrezje 2"> -Oresko nabrezje 2</option>
                 <option value="20.Meljska - trgovina"> -Meljska - trgovina</option>
                 <option value="20.Meljska - Partizanska"> -Meljska - Partizanska</option>
                 <option value="20.AP Mlinska"> -AP Mlinska</option>
                 <option value="20.City center"> -City center</option>
                 <option value="20.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value="20.Glavni trg - zidovska"> -Glavni trg - zidovska</option>
                 <option value="20.Magdalena"> -Magdalena</option>
                 <option value="20.Pobreska - Europark"> -Pobreska - Europark</option>
                 <option value="20.Greenwich"> -Greenwich</option>
                 <option value="20.cufarjeva - TVD Partizan"> -cufarjeva - TVD Partizan</option>
                 <option value="20.cufarjeva - dom st. obcanov"> -cufarjeva - dom st. obcanov</option>
                 <option value="20.Meljski hrib - most"> -Meljski hrib - most</option>
                 <option value="20.Kovacic"> -Kovacic</option>
                 <option value="20.Lorber"> -Lorber</option>
                 <option value="20.Malecnik - odcep Trcova"> -Malecnik - odcep Trcova</option>
                 <option value="20.Malecnik - trgovina"> -Malecnik - trgovina</option>
                 <option value="20.Trcova 31a"> -Trcova 31a</option>
                 <option value="20.Trcova - Gricek"> -Trcova - Gricek</option>
                 <option value="20.Novek"> -Novek</option>
                 <option value="20.Metava - Duplek krizisce"> -Metava - Duplek krizisce</option>
                 <option value="20.Metava - obracalisce"> -Metava - obracalisce</option>
                 <option value="20.Metava - Duplek krizisce"> -Metava - Duplek krizisce</option>
                 <option value="20.Novak"> -Novak</option>
                 <option value="20.Trcova - Gricek"> -Trcova - Gricek</option>
                 <option value="20.Trcova 31a"> -Trcova 31a</option>
                 <option value="20.Malecnik - trgovina"> -Malecnik - trgovina</option>
                 <option value="20.Malecnik - odcep Trcova"> -Malecnik - odcep Trcova</option>
                 <option value="20.Celestrina"> -Celestrina</option>
                 <option value="20.Nebova I"> -Nebova I</option>
                 <option value="20.Nebova II"> -Nebova II</option>
                 <option value="20.Ruperce"> -Ruperce</option>
                 <option value="20.Kronaveter"> -Kronaveter</option>
                 <option value="20.Knezar"> -Knezar</option>
                 <option value="20.Metava - odcep"> -Metava - odcep</option>
                 <option value="20.Zimica - odcep"> -Zimica - odcep</option>
                 <option value="20.Grusova 1"> -Grusova 1</option>                 
                 <option value="20.Grusova-obracalisce">  -Grusova-obracalisce </option>
                 
                
                 
                 <option id="bold"value="LINIJA 21 Ljubljanska-Trzaska c. - Merkur ">•LINIJA 21 Ljubljanska-Trzaska c. - Merkur</option>                 
                 <option value="21.Ul. kraljevica Marka"> -Ul. kraljevica Marka</option>
                 <option value="21.Oresko nabrezje 1"> -Oresko nabrezje 1</option>
                 <option value="21.Oresko nabrezje 2"> -Oresko nabrezje 2</option>
                 <option value="21.AP Mlinska"> -AP Mlinska</option>
                 <option value="21.City center"> -City center</option>
                 <option value="21.Kneza Koclja - Vetrinjska"> -Kneza Koclja - Vetrinjska</option>
                 <option value="21.Magdalenski park"> -Magdalenski park</option>
                 <option value="21.Ljubljanska - Pariske komune"> -Ljubljanska - Pariske komune</option>
                 <option value="21.Ljubljanska - Focheva"> -Ljubljanska - Focheva</option>
                 <option value="21.Ljubljanska - trgovski center"> -Ljubljanska - trgovski center</option>
                 <option value="21.Ljubljanska - stolpnica"> -Ljubljanska - stolpnica</option>
                 <option value="21.Ljubljanska 140"> -Ljubljanska 140</option>
                 <option value="21.Ul. Eve Lovse - Betnava"> -Ul. Eve Lovse - Betnava</option>
                 <option value="21.Ul. Eve Lovse - Energetika"> -Ul. Eve Lovse - Energetika</option>
                 <option value="21.Ul. Eve Lovse - Lidl"> -Ul. Eve Lovse - Lidl</option>
                 <option value="21.Trzaska cesta - Elko"> -Trzaska cesta - Elko</option>
                 <option value="21.Trzaska cesta - Semenarna"> -Trzaska cesta - Semenarna</option>
                 <option value="21.Trzaska cesta - Carinarnica"> -Trzaska cesta - Carinarnica</option>
                 <option value="21.Trzaska cesta - Rutar"> -Trzaska cesta - Rutar</option>
                 <option value="21.E'Leclerc - obracalisce"> -E'Leclerc - obracalisce</option>
                 <option value="21.Trzaska cesta - Bauhaus"> -Trzaska cesta - Bauhaus</option>
                 <option value="21.Trzaska cesta - Merkur">  -Trzaska cesta - Merkur </option>
            </select>
                  <br/><input type="submit" value="Potrdi" name="submit" id="gumb" />
      
        </form>   
             
    </body>
      </div>
    </div>
    <div id="footer">
      Copyright &copy; MMP 
          <script type="text/javascript">
             document.write ('<p><span id="date-time">', new Date().toLocaleString(), '<\/span>.<\/p>')
             if (document.getElementById) onload = function () {
	               setInterval ("document.getElementById ('date-time').firstChild.data = new Date().toLocaleString()", 50)
}
</script>
    </div>
  </div>
    </body>
</html>
